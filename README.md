# Code for Vadde and Russell et al. 2024: The transcription factor ATML1 maintains giant cell identity by inducing synthesis of its own very long-chain fatty acid ligands 

bioRxiv Preprint: https://doi.org/10.1101/2024.03.14.584694 

## Description
This repository contins the code from Vadde and Russell et al. (2024).

## Contributing
Contributers to this code are:

Nicholas J. Russell (nrussell@mpipz.mpg.de)

Pau Formosa-Jordan (pformosa@mpipz.mpg.de)

Michael Saint-Antoine (mikest@udel.edu)

We thank Henrik Jonnson for his advice and the team of people who helped make [Tissue](https://gitlab.com/slcu/teamHJ/tissue). 

## License

MIT License

Copyright (c) 2024, Nicholas Russell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
