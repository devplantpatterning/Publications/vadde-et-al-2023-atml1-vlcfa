We began with a normalized expression dataset (called “expression_data.csv” in the code repository), containing TPM measurements for 27,655 genes under four different conditions: a control condition, a 0.1uM induction of ATML1, a 1uM induction of ATML1, and a 10uM induction of ATML1. For each ATML1 induction condition, measurements were taken at four time points: 8hr, 16hr, 24hr, and 32hr (except for the 10uM condition, which does not have a 16hr measurement). For the control condition, measurements were taken at only the 0hr time point. For each condition at each time point, measurements were taken for three replications.

To predict potential targets of ATML1, we calculated the Spearman correlation between ATML1 and every other gene across the 24hr and 32hr time points, and selected the genes that were significantly correlated to ATML1 with a Bonferroni-corrected p-value less than or equal to 0.05. The code for this correlation analysis can be found in the “get_spearman_correlations.py” file in the code repository.

This selection process yielded 141 potential target genes, including 110 positively correlated with ATML1 and 31 negatively correlated with ATML1. 

From time series plots of TPM, we visually inspected the expression of these genes relative to ATML1 to check if they follow the same pattern (which would suggest a possible regulatory relationship). The plots for these correlated genes can be found in the folder called “timeseries_plots” in the code repository, and the code for generating the plots can be found in the “timeseries_plots.py” file.

In addition to the time series plots, we also generated scatter plots of ATML1 expression against potential target gene expression using data from the 24hr and 32hr timepoints. We then fit Hill functions to these plots, to gain insight into possible regulatory dynamics. 

We used the following Hill function to model positive regulation between ATML1 and a target: 

$$target\left(ATML1,k,c,h,b\right)  = k \dfrac{ ATML1^h}{c^h + ATML1^h} + b$$

Here, the target expression is represented as a function of the ATML1 expression and four constant parameters: k, c, h, and b, which are described below:

- $b$ represents a basal expression level of the target that occurs even when there is no ATML1 expression.
- $k$ defines the maximum level of target expression that can be activated by ATML1. That is, with more and more ATML1 expression, the fraction  approaches 1, and the term  approaches .
- $h$ is the “Hill coefficient”. This parameter controls the steepness of the response curve of the target expression to ATML1 expression. The greater h is, the steeper the response curve is. In biological terms, this parameter may help us to distinguish between linear activation of the target (h close to 1) and threshold-based activation (greater h value).
- $c$ is another constant parameter that determines the saturation of  .

As ATML1 expression increases, the target expression increases. The relationship between the two expression levels can be linear or logistic (saturating at some point), or in between, depending on the parameters c and h. The parameter b represents a basal expression rate of the target that occurs when there is no ATML1 expression.

In order to fit our model to the data, we defined an objective function as follows:



Here,  and  represent the data points we have for ATML1 and target expression, and  represents the model prediction for target expression given a parameter set. So, for a given set of values for k, c, h, and b, this objective function quantifies the total squared error between our model predictions and the data. 

In order to fit our model to the data, we applied a numerical optimization in Python to find the values of k, c, h, and b that minimized the objective function. The code for this process is available in the “hill_fit.py” file in the code repository, and the plots are available in the “hill_plots” folder.

In addition to modeling positive regulation, we can also defined a Hill function to model negative regulation between ATML1 and a target:

This function is similar to the positive Hill function, except that the target expression decreases with increasing ATML1 expression.

We then used the same objective function and minimization strategy to fit the negative Hill function to the data. These plots are also found in the “hill_plots” folder, along with the plots with positive Hill functions.
