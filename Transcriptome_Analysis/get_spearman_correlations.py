import numpy as np
import csv
import scipy.stats
from operator import itemgetter





data = []
genes = []
with open("expression_data.csv") as csvfile:

    reader = csv.reader(csvfile, delimiter=',')

    for row in reader:

        # read in gene ID
        genes.append(row[0])

        # read in data for 24hr and 32hr timepoints
        data.append(row[25:])




# first entry is the column heading, need to remove
genes = genes[1:]

# sample labels
samps = data[0]


# cast to floats, exclude first row which is the column headings
data = np.array(data[1:]).astype(float)




# some basic checks to make sure data was read in correctly
assert len(genes) == data.shape[0] # should be 27656 (27655 genes + 1 GFP_coding_seq at the end)
assert len(samps) == data.shape[1] # should be 23




## reading in IDs of VLCFA genes, to check if any show up as correlated to ATML1
vlcfa_genes = []
with open("vlcfa_list.csv") as csvfile:

    reader = csv.reader(csvfile, delimiter=',')
    next(reader)

    for row in reader:
        vlcfa_genes.append(row[0])

# get rid of duplicates
vlcfa_genes = list(set(vlcfa_genes))




# significant p value (before Bonferroni correction)
alpha = 0.05

# number of genes
N = len(genes)

# ATML1 index. (ATML1 ID is AT4G21750)
atml1_ind = 19102


# empty lists to save indices of genes significantly correlated with ATML1
# and the signs of the correlations
inds_to_save = []
signs = []

# for counting positive and negative significant correlations
pos_count = 0
neg_count = 0

# for writing full output
to_write = []

# list of significant genes
sig_genes = []

# list of significant VLCFA genes
sig_VLCFA_genes = []

for target_ind in range(len(genes)):

    if target_ind != atml1_ind:

        # get spearman correlation and p value for ATML1 and each possible target
        cor, p_value = scipy.stats.spearmanr(data[atml1_ind,:],data[target_ind,:])

        # Bonferroni correction
        corrected_p = p_value * N


        # check if the potentially target gene is on the VLCFA list
        if genes[target_ind] in vlcfa_genes:
            vlcfa_label = "YES"
        else:
            vlcfa_label = "NO"



        # check for significance with Bonferroni correction
        if corrected_p <= alpha:

            # going to save inds of significantly correlated genes
            inds_to_save.append(target_ind)


            if cor > 0:
                # positive correlation
                signs.append(1)
                pos_count += 1

            elif cor < 0:
                # negative correlation
                signs.append(-1)
                neg_count += 1
            # pretty sure none are exactly 0. but if one shows up, this code will throw an assertion error later on


            # save information to be written to output CSV
            to_write.append([genes[atml1_ind],genes[target_ind],cor,p_value,corrected_p,"YES",vlcfa_label])

            # save significant gene, to write to output CSV
            sig_genes.append(genes[target_ind])

            # save significant VLCFA gene, to write to output CSV
            if vlcfa_label == "YES":
                sig_VLCFA_genes.append(genes[target_ind])

        else:
            # save information to be written to output CSV, but note that it is NOT significantly correlated
            to_write.append([genes[atml1_ind],genes[target_ind],cor,p_value,corrected_p,"NO",vlcfa_label])

    

# check that we have a sign for every significant correlation
assert len(inds_to_save) == len(signs)



# print quick summary of results
print()
print()
print(f"significant positive correlations: {pos_count}")
print(f"significant negative correlations: {neg_count}")
print(f"total: {pos_count + neg_count}")
print()
print(f"correlated VLCFA genes: {len(sig_VLCFA_genes)}")



# saving indices and signs of significantly correlated genes to numpy objects
np.save('inds_to_save_24_and_32.npy', inds_to_save) 
np.save('signs_24_and_32.npy', signs) 



########################################################################################
## Getting gene names for output files

# so far we've been working with the gene IDs.
# need to start working with the actual gene names now.
############################################


def get_gene_name(string_in):

    # bit of string parsing to get the gene names

    start = -1
    end = -1

    for i in range(len(string_in)):
        if string_in[i]=="(":
            start = i
        elif string_in[i]==")":
            end = i

    if start >=0:
        return string_in[start:end+1]
    else:
        return("")


# saving ID-name mapping to a dictionary for quick lookup when needed
name_dict = {}
with open("gene_names.csv") as csvfile:

    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        name_dict[row[0]] = get_gene_name(row[1])[1:-1]

########################################################################################




# writing 3 output files:
# spearman_correlations.csv -- general information about all genes that were checked
# correlated_genes.csv -- list of all significantly correlated genes
# correlated_VLCFA_genes.csv -- list of significantly correlated VLCFA genes


to_write = sorted(to_write, key=itemgetter(5), reverse=True)

# get gene names for output file
gene1_names = []
gene2_names = []

for i in range(len(to_write)):
    gene1_names.append(name_dict[to_write[i][0]])
    gene2_names.append(name_dict[to_write[i][1]])

with open("output/spearman_correlations.csv", "w") as record_file:
    record_file.write("gene1_ID,gene1_name,gene2_ID,gene2_name,spearman_correlation,raw_p_value,corrected_p_value,significant_after_bonferroni_correction,is_VLCFA_gene\n")

    for i in range(len(to_write)):
        record_file.write(f"{to_write[i][0]},{gene1_names[i]},{to_write[i][1]},{gene2_names[i]},{to_write[i][2]},{to_write[i][3]},{to_write[i][4]},{to_write[i][5]},{to_write[i][6]}\n")


sig_gene_names = []
for i in range(len(sig_genes)):
    sig_gene_names.append(name_dict[sig_genes[i]])

with open("output/correlated_genes.csv", "w") as record_file:
    record_file.write("gene_ID,gene_name\n")
    for i in range(len(sig_genes)):
        record_file.write(f"{sig_genes[i]},{sig_gene_names[i]}\n")


sig_VLCFA_gene_names = []
for i in range(len(sig_VLCFA_genes)):
    sig_VLCFA_gene_names.append(name_dict[sig_VLCFA_genes[i]])

with open("output/correlated_VLCFA_genes.csv", "w") as record_file:
    record_file.write("gene_ID,gene_name\n")
    for i in range(len(sig_VLCFA_genes)):
        record_file.write(f"{sig_VLCFA_genes[i]},{sig_VLCFA_gene_names[i]}\n")
