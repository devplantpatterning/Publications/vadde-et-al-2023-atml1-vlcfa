import numpy as np
import csv
import matplotlib.pyplot as plt
import random



# requirements for journal
dpi = 300
label_font = {'family':'arial','size':14}

alpha_to_use = 0.5 # matplotlib parameter for plots. not related to statistical significance


random.seed(12)
## going to pick random uncorrelated genes to plot as controls
## seeding random number generator so results can be replicated exactly


# set to True to make plots for uncorrelated controls
uncorrelated_control = False


# load correlated inds
inds_to_save = list(np.load('inds_to_save_24_and_32.npy'))




# get uncorrelated controls if needed
if uncorrelated_control:
    rands = []
    for i in range(100):
        rand = random.randint(0,27656)

        if rand not in inds_to_save and rand not in rands:
            rands.append(rand)
    inds_to_save = rands



########################################################################################
## Getting gene names

# so far we've been working with the gene IDs.
# need to start working with the actual gene names now.
############################################


def get_gene_name(string_in):

    # bit of string parsing to get the gene names

    start = -1
    end = -1

    for i in range(len(string_in)):
        if string_in[i]=="(":
            start = i
        elif string_in[i]==")":
            end = i

    if start >=0:
        return string_in[start:end+1]
    else:
        # if no name, just return ""
        return("")


# saving ID-name mapping to a dictionary for quick lookup when needed
name_dict = {}
with open("gene_names.csv") as csvfile:

    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        name_dict[row[0]] = get_gene_name(row[1])[1:-1]

########################################################################################



# function to take in expression data and return mean and standard deviation
def get_stats(array_in, inds):

    tmp = []
    for i in inds:
        tmp.append(array_in[i])

    mean = np.mean(tmp)
    std = np.std(tmp)

    return(mean,std)




data = []
genes = []
with open("expression_data.csv") as tsvfile:

    reader = csv.reader(tsvfile, delimiter=',')
    for row in reader:

        genes.append(row[0])
        data.append(row[1:])



# first entry is the column heading, need to remove
genes = genes[1:]

# sample labels
samps = data[0]


# cast to floats, exclude first row which is the column headings
data = np.array(data[1:]).astype(float)





## inds for different timepoints and experimental conditions of data
# Ohr, 8hr, 16hr, 24hr, 32hr

inds_0hr = [0,1,2]

inds_mock_8hr = [3,4,5]
inds_01um_8hr = [6,7,8]
inds_1um_8hr = [9,10,11]
inds_10um_8hr = [12,13,14]

inds_mock_16hr = [15,16,17]
inds_01um_16hr = [18,19,20]
inds_1um_16hr = [21,22,23]


inds_mock_24hr = [24,25,26]
inds_01um_24hr = [27,28,29]
inds_1um_24hr = [30,31,32]
inds_10um_24hr = [33,34]



inds_mock_32hr = [35,36,37]
inds_01um_32hr = [38,39,40]
inds_1um_32hr = [41,42,43]
inds_10um_32hr = [44,45,46]


## description of exactly what each index is in the dataset

# 0# ATML1ind_0hr_rep1_TPM
# 1# ATML1ind_0hr_rep2_TPM
# 2# ATML1ind_0hr_rep3_TPM
# 3# ATML1ind_Mock_rep1_8hr_TPM
# 4# ATML1ind_Mock_rep2_8hr_TPM
# 5# ATML1ind_Mock_rep3_8hr_TPM
# 6# ATML1ind_0.1uM_rep1_8hr_TPM
# 7# ATML1ind_0.1uM_rep2_8hr_TPM
# 8# ATML1ind_0.1uM_rep3_8hr_TPM
# 9# ATML1ind_1uM_rep1_8hr_TPM
# 10# ATML1ind_1uM_rep2_8hr_TPM
# 11# ATML1ind_1uM_rep3_8hr_TPM
# 12# ATML1ind_10uM_rep1_8hr_TPM
# 13# ATML1ind_10uM_rep2_8hr_TPM
# 14# ATML1ind_10uM_rep3_8hr_TPM
# 15# ATML1ind_Mock_rep1_16hr_TPM
# 16# ATML1ind_Mock_rep2_16hr_TPM
# 17# ATML1ind_Mock_rep3_16hr_TPM
# 18# ATML1ind_0.1uM_rep1_16hr_TPM
# 19# ATML1ind_0.1uM_rep2_16hr_TPM
# 20# ATML1ind_0.1uM_rep3_16hr_TPM
# 21# ATML1ind_1uM_rep1_16hr_TPM
# 22# ATML1ind_1uM_rep2_16hr_TPM
# 23# ATML1ind_1uM_rep3_16hr_TPM
# 24# ATML1ind_Mock1_rep1_24hr_TPM
# 25# ATML1ind_Mock2_rep2_24hr_TPM
# 26# ATML1ind_Mock3_rep3_24hr_TPM
# 27# ATML1ind_0.1uM_rep1_24hr_TPM
# 28# ATML1ind_0.1uM_rep2_24hr_TPM
# 29# ATML1ind_0.1uM_rep3_24hr_TPM
# 30# ATML1ind_1uM_rep1_24hr_TPM
# 31# ATML1ind_1uM_rep2_24hr_TPM
# 32# ATML1ind_1uM_rep3_24hr_TPM
# 33# ATML1ind_10uM_rep2_24hr_TPM
# 34# ATML1ind_10uM_rep3_24hr_TPM
# 35# ATML1ind_Mock_rep1_32hr_TPM
# 36# ATML1ind_Mock_rep2_32hr_TPM
# 37# ATML1ind_Mock_rep3_32hr_TPM
# 38# ATML1ind_0.1uM_rep1_32hr_TPM
# 39# ATML1ind_0.1uM_rep2_32hr_TPM
# 40# ATML1ind_0.1uM_rep3_32hr_TPM
# 41# ATML1ind_1uM_rep1_32hr_TPM
# 42# ATML1ind_1uM_rep2_32hr_TPM
# 43# ATML1ind_1uM_rep3_32hr_TPM
# 44# ATML1ind_10uM_rep1_32hr_TPM
# 45# ATML1ind_10uM_rep2_32hr_TPM
# 46# ATML1ind_10uM_rep3_32hr_TPM


for gene_index in inds_to_save:

    # this whole section is a bit cumbersome.
    # basically just plotting expression at each timepoint/experimental condition
    # first for ATML1, then for potential target gene (or uncorrelated control)
    # then we can visually inspect the plots to see how expression of potential target is changing as ATML1 expression changes
    # plots show mean expression across replicates, bars show standard deviation.
    # if you want to plot standard error or confidence interval rather than standard deviation, change the get_stats function accordingly


    ####### FIRST STEP -- MAKE PLOT FOR ATML1


    ## ATML1 index is 19102
    atml1_ind = 19102

    gene_data = data[atml1_ind,:]



    inds_lists1 = [inds_0hr,inds_mock_8hr,inds_mock_16hr,inds_mock_24hr,inds_mock_32hr]

    inds_lists2 = [inds_01um_8hr,inds_01um_16hr,inds_01um_24hr,inds_01um_32hr]

    inds_lists3 = [inds_1um_8hr,inds_1um_16hr,inds_1um_24hr,inds_1um_32hr]

    inds_lists4 = [inds_10um_8hr,inds_10um_24hr,inds_10um_32hr]


    ## using first subplot
    plt.subplot(2, 1, 1)



    means = []
    stds = []
    for inds in inds_lists1:
        gene_mean, gene_std = get_stats(gene_data, inds)

        means.append(gene_mean)
        stds.append(gene_std)

    t = [0,8,16,24,32]
    plot1 = plt.errorbar(t, means, yerr=stds, fmt="o",color='blue',label="Mock",alpha=alpha_to_use)
    #################
    #################
    means = []
    stds = []
    for inds in inds_lists2:
        gene_mean, gene_std = get_stats(gene_data, inds)

        means.append(gene_mean)
        stds.append(gene_std)

    t = [8,16,24,32]
    plot2 = plt.errorbar(t, means, yerr=stds, fmt="o",color='red',label="0.1um",alpha=alpha_to_use)
    #################
    #################
    means = []
    stds = []
    for inds in inds_lists3:
        gene_mean, gene_std = get_stats(gene_data, inds)

        means.append(gene_mean)
        stds.append(gene_std)

    t = [8,16,24,32]
    plot3 = plt.errorbar(t, means, yerr=stds, fmt="o",color='orange',label="1um",alpha=alpha_to_use)
    #################
    #################
    means = []
    stds = []
    for inds in inds_lists4:
        gene_mean, gene_std = get_stats(gene_data, inds)

        means.append(gene_mean)
        stds.append(gene_std)

    t = [8,24,32]
    plot4 = plt.errorbar(t, means, yerr=stds, fmt="o",color='black',label="10um",alpha=alpha_to_use)

    #################
    #################

    plt.legend(handles=[plot1,plot2,plot3,plot4],fontsize=8)

    plt.xticks([0,8,16,24,32])

    plt.title("ATML1",fontdict = label_font)



    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################
    #######################################################################################

    ## SECOND STEP -- MAKE PLOT FOR OTHER GENE


    # gene index is coming from for-loop, iterating over indexes of genes that had significant spearman correlation
    gene_data = data[gene_index,:]


    ## using second subplot now, basically doing the same plotting we did for ATML1
    plt.subplot(2, 1, 2)


    means = []
    stds = []
    for inds in inds_lists1:
        gene_mean, gene_std = get_stats(gene_data, inds)

        means.append(gene_mean)
        stds.append(gene_std)

    t = [0,8,16,24,32]
    plot1 = plt.errorbar(t, means, yerr=stds, fmt="o",color='blue',label="Mock",alpha=alpha_to_use)
    #################
    #################
    means = []
    stds = []
    for inds in inds_lists2:
        gene_mean, gene_std = get_stats(gene_data, inds)

        means.append(gene_mean)
        stds.append(gene_std)

    t = [8,16,24,32]
    plot2 = plt.errorbar(t, means, yerr=stds, fmt="o",color='red',label="0.1um",alpha=alpha_to_use)
    #################
    #################
    means = []
    stds = []
    for inds in inds_lists3:
        gene_mean, gene_std = get_stats(gene_data, inds)

        means.append(gene_mean)
        stds.append(gene_std)

    t = [8,16,24,32]
    plot3 = plt.errorbar(t, means, yerr=stds, fmt="o",color='orange',label="1um",alpha=alpha_to_use)
    #################
    #################
    means = []
    stds = []
    for inds in inds_lists4:
        gene_mean, gene_std = get_stats(gene_data, inds)

        means.append(gene_mean)
        stds.append(gene_std)

    t = [8,24,32]
    plot4 = plt.errorbar(t, means, yerr=stds, fmt="o",color='black',label="10um",alpha=alpha_to_use)

    #################
    #################

    plt.legend(handles=[plot1,plot2,plot3,plot4],fontsize=8)

    plt.xticks([0,8,16,24,32])

    gene_name_lookup = name_dict[genes[gene_index]]

    if gene_name_lookup=="":
        plt.title(genes[gene_index],fontdict = label_font)
        savefile_name = genes[gene_index]

    else:
        plt.title(gene_name_lookup,fontdict = label_font)
        savefile_name = gene_name_lookup



    plt.xlabel("Time (hours)",fontdict = label_font)
    plt.ylabel("TPM",fontdict = label_font)
       





    ## some plot formatting
    plt.subplots_adjust(left=0.11,
                        bottom=0.1, 
                        right=0.9, 
                        top=0.9, 
                        wspace=0.4, 
                        hspace=0.4)


    # save PDF plots
    if uncorrelated_control:
        plt.savefig("uncorrelated_timeseries_plots/"+savefile_name+".pdf",dpi=dpi)

    else:
        plt.savefig("timeseries_plots/"+savefile_name+".pdf",dpi=dpi)


    # # save JPEG plots -- uncomment if needed
    # if uncorrelated_control:
    #     plt.savefig("uncorrelated_timeseries_plots/"+savefile_name+".jpeg",dpi=dpi)

    # else:
    #     plt.savefig("timeseries_plots/"+savefile_name+".jpeg",dpi=dpi)

    plt.close()
