import numpy as np
import csv
import matplotlib.pyplot as plt
import random
import sys

## seeding random number generators so results can be replicated exactly
random.seed(13)
np.random.seed(14)

# requirements for journal
dpi = 300
label_font = {'family':'arial','size':14}




def hill_function(atml1, h, c, k, b):
    # basic Hill function to attempt to predict target expression from ATML1 expression

    target_exp = k * atml1**h / (c**h + atml1**h) + b

    return(target_exp)



def negative_hill_function(atml1, h, c, k, b):
    # negative Hill function to attempt to predict target expression from ATML1 expression

    target_exp = k  / (c**h + atml1**h) + b

    return(target_exp)






def plot_hill(atml1_max, params):
    # plot Hill curve, given parameters
    # going to be used later on to plot the curves with the fit parameters

    h = params[0]
    c = params[1]
    k = params[2]
    b = params[3]

    atml1_exp = list(np.linspace(0,atml1_max,num=101))

    target_exp = []

    for item in atml1_exp:
        result = hill_function(item, h, c, k, b)
        target_exp.append(result)


    plt.plot(atml1_exp, target_exp,color="blue")
    # plt.show()

def plot_negative_hill(atml1_max, params):
    # plot negative Hill curve, given parameters

    h = params[0]
    c = params[1]
    k = params[2]
    b = params[3]

    atml1_exp = list(np.linspace(0,atml1_max,num=101))

    target_exp = []

    for item in atml1_exp:
        result = negative_hill_function(item, h, c, k, b)
        target_exp.append(result)


    plt.plot(atml1_exp, target_exp,color="blue")
    # plt.show()





def loss_function(params, atml1_exp, target_exp):

    # squared error loss function for positive Hill 
    # going to be used to fit Hill parameters


    h = params[0]
    c = params[1]
    k = params[2]
    b = params[3]

    loss = 0

    for i in range(len(target_exp)):

        model_prediction = hill_function(atml1_exp[i], h, c, k, b)


        res = (model_prediction - target_exp[i])**2

        loss += res

    return(loss)



def negative_loss_function(params, atml1_exp, target_exp):

    ## squared error loss function (negative Hill)



    h = params[0]
    c = params[1]
    k = params[2]
    b = params[3]

    loss = 0

    for i in range(len(target_exp)):

        model_prediction = negative_hill_function(atml1_exp[i], h, c, k, b)


        res = (model_prediction - target_exp[i])**2

        loss += res

    return(loss)





def fit_model(atml1_exp, target_exp):

    ## brute force minimization of loss function (positive Hill)
    # goal here is to simply try out 1000000 parameter sets, 
    # with parameters randomly chosen using reasonable bounds
    # then keep the parameters that give the lowest loss
    # will also need to visually confirm that the fit curves look correct in the plots



    for counter in range(1000000):


        h = random.uniform(1,20)
        c = random.uniform(0,200)
        k = random.uniform(0,np.max(target_exp))
        b = random.uniform(0,np.max(target_exp))


        current_params = [h, c, k, b]

        current_loss = loss_function(current_params, atml1_exp, target_exp)

        if counter == 0:
            best_loss = current_loss
            best_params = current_params
        else:
            if current_loss < best_loss:
                best_loss = current_loss
                best_params = current_params


    return(best_params)



def negative_fit_model(atml1_exp, target_exp):

    ## brute force minimization of loss function (negative Hill)
    # goal here is to simply try out 1000000 parameter sets, 
    # with parameters randomly chosen using reasonable bounds
    # then keep the parameters that give the lowest loss
    # will also need to visually confirm that the fit curves look correct in the plots





    for counter in range(1000000):

        h = random.uniform(1,10)
        c = random.uniform(1,1000)
        k = random.uniform(0,10000)
        b = random.uniform(0,np.min(target_exp))        

        current_params = [h, c, k, b]

        current_loss = negative_loss_function(current_params, atml1_exp, target_exp)

        if counter == 0:
            best_loss = current_loss
            best_params = current_params
        else:
            if current_loss < best_loss:
                best_loss = current_loss
                best_params = current_params


    return(best_params)








alpha_to_use = 0.7 # parameter for matplotlib plots, not related to statistical significance

## loading in indices and signs of genes significantly correlated with ATML1
inds_to_save = list(np.load('inds_to_save_24_and_32.npy'))
signs = list(np.load('signs_24_and_32.npy'))
assert len(inds_to_save) == len(signs)

########################################################################################
## Getting gene names

# so far we've been working with the gene IDs.
# need to start working with the actual gene names now.
############################################


def get_gene_name(string_in):

    # bit of string parsing to get the gene names

    start = -1
    end = -1

    for i in range(len(string_in)):
        if string_in[i]=="(":
            start = i
        elif string_in[i]==")":
            end = i

    if start >=0:
        return string_in[start:end+1]
    else:
        # if no name, just return ""
        return("")


# saving ID-name mapping to a dictionary for quick lookup when needed
name_dict = {}
with open("gene_names.csv") as csvfile:

    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        name_dict[row[0]] = get_gene_name(row[1])[1:-1]




########################################################################################






#24hr inds
#24:35 (not including 35)
start_24 = 24

#32hr inds
# 35:
start_32 = 35


short_data = [] # expression data for all timepoints
long_data = [] # expression data 24hr and 32hr only
genes = []
with open("expression_data.csv") as csvfile:


    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:

        genes.append(row[0])
        long_data.append(row[1:])
        short_data.append(row[1+start_24:])





genes = genes[1:]

samps = short_data[0]
short_data = np.array(short_data[1:]).astype(float)
long_data = np.array(long_data[1:]).astype(float)

# saving info to write output file at the end
gene_names_to_write = []
gene_ids_to_write = []
fit_params_to_write = []

for i in range(len(inds_to_save)):

    print(f"Percent complete: {round(i/len(inds_to_save)*100,3)}")

    # bit of cumbersome plotting code. basically just making a scatter plot of 
    # potential target gene expression vs atml1 expression at 24hr and 32hr.
    # the different timepoints are colorcoded 

    # Hill curve for fit parameters is also plotted

    # code is written so that it will be (hopefully) easy to make these same plots
    # for all the timepoints if you want to.


    sign = signs[i]
    gene_index = inds_to_save[i]


    atml1_ind = 19102

    atml1_data = short_data[atml1_ind,:]


    gene_data = short_data[gene_index,:]


    atml1_max = np.max(atml1_data)
    if sign==1:
        # positive Hill
        fit_params = fit_model(atml1_data, gene_data)
        plot_hill(atml1_max, fit_params)

    else:
        # negative Hill
        fit_params = negative_fit_model(atml1_data, gene_data)
        plot_negative_hill(atml1_max, fit_params)




    inds_mock_24hr = [24,25,26]
    inds_01um_24hr = [27,28,29]
    inds_1um_24hr = [30,31,32]
    inds_10um_24hr = [33,34]



    inds_mock_32hr = [35,36,37]
    inds_01um_32hr = [38,39,40]
    inds_1um_32hr = [41,42,43]
    inds_10um_32hr = [44,45,46]



    atml1_data = long_data[atml1_ind,:]





    gene_data = long_data[gene_index,:]



    inds24 = inds_mock_24hr + inds_01um_24hr + inds_1um_24hr + inds_10um_24hr 
    plot24 = plt.scatter(atml1_data[inds24], gene_data[inds24],alpha=alpha_to_use,color="red", label="24hr")

    inds32 = inds_mock_32hr + inds_01um_32hr + inds_1um_32hr + inds_10um_32hr 
    plot32 = plt.scatter(atml1_data[inds32], gene_data[inds32],alpha=alpha_to_use,color="gray",label="32hr")

    plt.legend(handles=[plot24, plot32],fontsize=14)




    fit_params = np.array(fit_params).round(decimals=1)




    params_for_title = f"; h={fit_params[0]}, c={fit_params[1]}, k={fit_params[2]}, b={fit_params[3]}"


    gene_name_lookup = name_dict[genes[gene_index]]


    plt.xlabel("ATML1 Expression",fontdict = label_font)

    if gene_name_lookup=="":
        plt.title(genes[gene_index]+ params_for_title,fontdict = label_font)
        plt.ylabel(genes[gene_index] + " Expression",fontdict = label_font)
        plt.savefig(f"hill_plots/{genes[gene_index]}.pdf",dpi=dpi)
        gene_names_to_write.append("")

    else:
        plt.title(name_dict[genes[gene_index]]+ params_for_title,fontdict = label_font)
        plt.ylabel(genes[gene_index] + " / " + gene_name_lookup + " Expression",fontdict = label_font)
        plt.savefig(f"hill_plots/{gene_name_lookup}.pdf",dpi=dpi)
        gene_names_to_write.append(gene_name_lookup)


    plt.close()

    gene_ids_to_write.append(genes[gene_index])
    fit_params_to_write.append(list(fit_params))




# writing output file with the fit parameters for each gene
with open("output/fit_hill_parameters.csv", "w") as record_file:
    record_file.write("gene_ID,gene_name,fit_h,fit_c,fit_k,fit_b\n")
    for i in range(len(gene_ids_to_write)):
        record_file.write(f"{gene_ids_to_write[i]},{gene_names_to_write[i]},{fit_params_to_write[i][0]},{fit_params_to_write[i][1]},{fit_params_to_write[i][2]},{fit_params_to_write[i][3]}\n")
