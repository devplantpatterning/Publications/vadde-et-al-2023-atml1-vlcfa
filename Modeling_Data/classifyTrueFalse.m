% Find true positives/negatives (TP/TN), false positives/negatives (FP/FN)
% Input - 2 populations (small and giant) to be classified; threshold
% Output - TP, TN, FP, FN

function [TP, TN, FP, FN] = classifyTrueFalse(small,giant,threshold)
    
    TP=0;
    TN=0;
    FP=0;
    FN=0;

    for i=1:size(small,1)
        
        if small(i)<=threshold
            
            TN=TN+1;
            
        else
            
            FP=FP+1;
            
        end
        
    end
    
    for i=1:size(giant,1)
        
        if giant(i)<=threshold
            
            FN=FN+1;
            
        else
            
            TP=TP+1;
            
        end
        
    end
                       
end