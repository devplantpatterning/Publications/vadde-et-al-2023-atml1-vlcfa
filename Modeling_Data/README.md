# Vadde et al 2023 - ATML1-VLCFA

# Mathematical Modeling and Simulations

In this paper, we have deterministic and stochastic simulations, showing the effects of adding a VLCFA synthesis inhibitor, cafenstrole, to plant tissue. This, in turn, affects the production of dimerized ATML1 and, downstream, the production of LGO, causing some giant cells to de-differentiate and divide. 

This code allows you to simulate both the determnistic (static and non-random) and the stochastic (growing and dividing tissue with noise) to produce the plots shown in the paper. All of the figures are reproducable. 

To run this code, you also must download Tissue (https://gitlab.com/slcu/teamHJ/tissue). The branch we recommend is multicell_nick. As well, we use MSEED 300, 400 and 500 (see myRandom.cc in Tissue).

## The Model

The full model is explained in the paper in the Methods section. 

In brief, we assumed that VLCFAs bind to ATML1, enabling ATML1 to dimerize.  We further assumed that only ATML1 dimers transcriptionally activate downstream gene expression, including activating biosynthesis of VLCFAs. We have previously shown that ATML1 operates in a weak positive feedback loop, activating its own expression, and this is now included in the model as the ATML1 dimer activating ATML1 expression. The cyclin dependent kinase (CDK) inhibitor, LOSS OF GIANT CELLS FROM ORGANS (LGO), acts downstream of ATML1 to induce endoreduplication of giant cells. In our model, the ATML1 dimer activates LGO expression, and if LGO concentration exceeds a predetermined threshold ($H_{T_1}$), the cell will endoreduplicate and become a giant cell.  We included the effect of the cafenstrole drug into our model. Specifically, cafenstrole impairs the basal production of VLCFAs as well as the activation of VLCFA synthesis by the ATML1 dimer.

To match the observed behavior of giant cells losing their fate and dividing in cafenstrole treated plants, we postulated a second threshold in LGO expression.  A giant cell’s fate will only be affected if  its LGO expression dips below a second threshold $H_{T_2] < H_{T_1}$. If this occurs at any time during the cell cycle, then the giant cell undergoes mitosis and divides by the end of the next G2-phase. These daughter cells divide yet again if, by the time they reach S-phase, their LGO levels are not above the second threshold HT2. If they are above the second threshold, the cell resumes endoreduplication. This second threshold $H_{T_2}$  allows a giant cell to de-differentiate and resume division. 

## The equations

The deterministic expressions for the dynamics of ATML1, VLCFA, dimerized ATML1, and LGO concentrations in cell $i$ can be written as follows, where $A = \left[\textit{ATML1}\right]$, $V = \left[\textit{VLCFA}\right]$, $B = \left[\textit{Dimerized ATML1}\right]$, and $L = \left[\textit{LGO}\right]$:

$$\frac{dA}{dt}  = 2K_{av}^-B - 2 K_{av}^+ V^2 A^2 + \dfrac{ \beta_a B^{n_a}}{K_a^{n_a} + B^{n_a}} + \beta_{A} - \nu_0 A$$

$$\frac{dB}{dt}  =  K_{av}^+ V^2 A^2 - \left[K_{av}^- + \nu_1\right] B $$

$$\frac{dV}{dt} = 2K_{av}^-B - 2 K_{av}^+ V^2 A^2 +  \frac{\beta_v B^{n_v}}{K_v^{n_v} + B^{n_v}}  + \beta_V - \nu_2 V $$

$$\frac{dL}{dt}  = \frac{\beta_{\ell} B^{n_{\ell}}}{K_{\ell}^{n_{\ell}} + B^{n_{\ell}}} - \nu_3 L + \beta_{L},$$


## Deterministic Simulations

To run deterministic simulations, please use the following templates:

- Main Run File: run_multicell_DeterminsitcSimulations.py 
- Solver File: rk (use RK4 and dt = 0.0001. Using an adaptive timestep is not recommended)
- Init File: SH_2Cell_1.init
- Model File: pre_multicell_ATML1-VLCFA_NoGrowDiv.model  

## Stochastic Simulations

To run stochastic simulations, please use the following templates:

- Main Run File: run_multicell_DeterminsitcSimulations.py
- Solver File: hi (use HuenItoLangevin with dt = 0.001. This allows for conservation of noise for the Mass Action)
- Init File: ideal_ic_Full_1.init (for the atml1-3 mutant, use ideal_ic_Full_atml13_i.init, as the ATML1 concentrations start off at 0)
- Model File: pre_multicell_ATML1-VLCFA_Growth_Division_ReverseEndo.model  
- MSEED: 300, 400, 500 (myRandom.cc in Tissue)

