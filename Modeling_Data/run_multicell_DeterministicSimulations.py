#!/usr/bin/env python

# This python script executes the ATML1 model in the tissue with deterministic simulations.
# At the moment, this script can run organism for different model files,
#and a single parameter set per model file. The results are generated and stored in a subfolder where this script is located.

################################################

#########################################################################################################################
# Parameters and strings to modify by the user
##############################################################################################################
##### Importing packages #####################################################################################
import matplotlib
matplotlib.use('Agg')

import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import pylab
import os
import shutil
import subprocess
import json
import pandas as pd
import scipy.io as sio
import generate_sim_files as gen # module to generate the simulation files

##############################################################################################################

# Strings to modify
rootsim='../tissue/bin/simulator' # path in which the tissue executable simulator is located
# Simulation parameters

algorithm='rk' #  used algorthim (eg., rk, rk5.rk, hi and gi). Note this model has been prepared for the 'rk' algorithm. Please do not use the RK5 time adaptive method.

model_name='ATML1-VLCFA_Determinstic' #No growth and division in this template

model_filename="multicell_"+model_name+".model"

string_pars='' # # substring of the subfolder where the results are going to be stored

dataname='idealtemplate1' # name of the subfolder in InitFiles where there are the initial conditions. After running the simulation, a folder will be created i n multicellTissue with this substring too, and the results are going to be allocated within this subdirectory.

initfilename='Two_Cells'; #Just two cells to simulate two different initial conditions.

list_ics=[1] # list of initial conditions
dt = 0.0001

sims=1          # sims=1 will run the simulations, otherwise set it to 0
D = 2/25 #This is the dilution term obtained from the stocahstic simulations.
DrugTime = int(20/dt) #When the drug will start being produced
#Production Terms.
#betaA = 0.2 #atml13 basal production of mutant
betaA = 1.1 #WT basal production of ATML1
#betaA = 10 #ATML1OE basal production of  ATML1
betaV = 1.1 #Basal VLCFA Production
betaL = 10 #WT Basal LGO/Target Production
betaC = 0.0 #No Cafenstrole Production
#betaC = 300.0 #Max Cafenstrole Production

#Degredation Terms:
nuzero = 1.0 + D #Degredation rate for ATML1
nuone =  0.4 + D #Degredation rate for VLCFA-ATML1 Dimer (B in the model)
nutwo = 0.03 + D #Degredation rate for VLCFA
nuthree = 9 + D #Degredation rate for LGO/Target
nuC = 0 #Degredation rate for Cafenstrole

#Mass Action Terms
Kavplus = 15 #Rate at which 2A + 2V -> B occurs
Kavminus = 12 #Rate at which B -> 2A + 2V occurs

#Hill Function terms

#ATML1dimer promotes ATML1#z
betaa = 2.0 #Max production term for ATML1
Ka = 0.7  #Hill Constant for ATML1
na = 2 #Hill Coefficinet for ATML1

#ATML1dimer promotes VLCFA#
betav = 1.4 #Max production term for VLCFA
Kv = 0.6 #Hill Constant for VLCFA
nv = 1 # Hill Coefficinet for VLCFA

#ATML1dimer promotes LGO/Target#
betaell = 8.0 #Max production term for LGO
Kell = 1.4 #Hill Constant for LGO
nell = 7 #Hill Coefficinet for LGO

#Cafenstrole blocks basal production of VLCFA and prudction from dimerized ATML1)
Kc = 30 #Hill Constant for Cafenstrole
nc = 5 #Hill Coefficinet for Cafenstrole

targetthres = 1.8
target2thres = 0.9
thsphase = 2.7

TimerRate = .45
TimerThres = 3.2
TimerThres2 = 3.2
# Strings to modify
string_sim='Determinstic_WT_{0}'.format(nutwo) # Substring of the subfolder where the results are going to be stored

# Generating a dictionary of parameters and storing them in a file
#KAVP = np.linspace(0,.2,20)
#ind=0
for ics in range(len(list_ics)) :
	modelpars_dict={'model_pars':{'dt': dt,
								  'betaA': betaA,
								  'betaV': betaV,
								  'betaC': betaC,
								  'betaL': betaL,
								  'nuzero': nuzero,
								  'nuone' : nuone,
								  'nutwo': nutwo,
								  'nuthree' : nuthree,
								  'nuC': nuC,
								  'Kavplus': Kavplus,
								  'Kavminus' : Kavminus,
								  'betaa' : betaa,
								  'Ka': Ka,
								  'na': na,
								  'betav' : betav,
								  'Kv': Kv,
								  'nv': nv,
								  'betaell' : betaell,
								  'Kell': Kell,
								  'nell': nell,
								  'betaV' : betaV,
		 					  	  'betav' : betav,
								  'Kc': Kc,
								  'nc': nc,
								  'DrugTime':DrugTime,
                                  'targetthres':targetthres,
                                  'target2thres':target2thres,
                                  'thsphase':thsphase,
								  'TimerRate':TimerRate,
								  'TimerThres':TimerThres,
								  'TimerThres2':TimerThres2}}

	modelfile_varying_pars_dict={'modelfile_varying_pars':{'dt':'dt',
								  'betaA': 'betaA',
								  'betaV': 'betaV',
								  'betaC': 'betaC',
								  'betaL': 'betaL',
								  'nuzero': 'nuzero',
								  'nuone' : 'nuone',
								  'nutwo': 'nutwo',
								  'nuthree' : 'nuthree',
								  'nuC': 'nuC',
								  'Kavplus': 'Kavplus',
								  'Kavminus' : 'Kavminus',
								  'betaa' : 'betaa',
								  'Ka': 'Ka',
								  'na': 'na',
								  'betav' : 'betav',
								  'Kv': 'Kv',
								  'nv': 'nv',
								  'betaell' : 'betaell',
								  'Kell': 'Kell',
								  'nell': 'nell',
								  'betaV' : 'betaV',
		 					  	  'betav' : 'betav',
								  'Kc': 'Kc',
								  'nc': 'nc',
								  'DrugTime':'DrugTime',
                                  'targetthres':'targetthres',
                                  'target2thres':'target2thres',
                                  'thsphase':'thsphase',
								  'TimerRate':'TimerRate',
								  'TimerThres':'TimerThres',
								  'TimerThres2':'TimerThres2'}}


	strings_to_change_dict={'strings_to_change':{'dirini':'kplu',
												'dataname_out':'lalelilolu',
												'subfolder_out':'lele'}}

	info={'string_pars': string_pars}
	info.update(modelpars_dict)
	info.update(modelfile_varying_pars_dict)
	info.update(strings_to_change_dict)
	with open('info.json','w') as f:
		json.dump(info, f)

	dirini=os.getcwd()  # storing the path in which the current script file is locate

	# creating the output folder in case it does not exist
	dataname_out='Results_'+dataname+'_'+initfilename
	try:
		os.stat(dataname_out)
	except:
		os.mkdir(dataname_out) # creating the directory

	strings_dict={'strings':{'model_name':model_name,
						'model_filename':model_filename,
						'dirini':dirini,
						'dataname_out': dataname_out}}

	info.update(strings_dict)

	os.chdir(dirini)
	val_ic=list_ics[ics]

	initfile='InitFiles/'+dataname+'/'+initfilename+'_'+str(val_ic)+'.init'

	shutil.copyfile(initfile,dataname_out+'/'+initfilename+'_'+str(val_ic)+'.init')	# moving data init file into the output folder

	subfolder_out=string_sim+model_name+'_'+algorithm+'_pars_'+string_pars+"_ic_"+str(val_ic)
	newstrings={'subfolder_out':subfolder_out}
	info['strings'].update(newstrings)

	# simulation loop
	if (sims==1) :
		outfile=model_name+'_'+algorithm+'_num_out'
		dirout = dataname_out+'/'+subfolder_out
		try:
			os.stat(dirout)
		except:
			os.mkdir(dirout)
			#print('Created directory '+dirout)
		diroutvtk = dataname_out+'/'+subfolder_out+'/vtk'
		diroutvtk = 'vtk'

		try:
			os.stat(diroutvtk)
		except:
			os.mkdir(diroutvtk)

		# Generation of the model file
		gen.generate_model_file(info)

		#print(command0)
		modelfile='ModelFiles/'+model_filename
		initfile='InitFiles/'+dataname+'/'+initfilename+'_'+str(val_ic)+'.init'
		solverfile='SolverFiles/'+algorithm
		outfile='num_out'

		cmd=rootsim+' '+modelfile+' '+initfile+' '+solverfile+' > '+dirout+'/'+outfile  # command line for running organism
		print('Running '+cmd)
		subprocess.call(cmd,shell=True)
		shutil.copyfile(modelfile,dirout+'/'+model_filename)		# copying model file
		shutil.move('tissue.gdata',dirout+'/tissue.gdata')					    # moving output to the results subfolder
		if os.path.exists('neighbours.gdata'):
			shutil.move('neighbours.gdata',dirout+'/neighbours.gdata')

		shutil.copyfile(initfile,dirout+'/'+initfilename+'_'+str(val_ic)+'.init')				# copying init file to the results subfolder

		if os.path.exists(dirout+'/vtk'):					# creating a folder where the vtk files are going to  be moved.
			shutil.rmtree(dirout+'/vtk')
		shutil.move('vtk',dirout+'/vtk')
		shutil.move(dirout+'/tissue.gdata',
		dirout+'/tissue.gdata')				    # moving output to the results subfolder
				# moving vtk output to the results subfolder
	# plotting loop
	os.chdir(dirini)
	subfolder_out=string_sim+model_name+'_'+algorithm+'_pars_'+string_pars+"_ic_"+str(val_ic)
	dirout = dataname_out+'/'+subfolder_out
	shutil.copyfile('analysis_multicellTissue.py',dirout+'/analysis_multicellTissue.py')
	shutil.copyfile('find_maxima.py',dirout+'/find_maxima.py')
	shutil.copyfile('info.json',dirout+'/info.json')
