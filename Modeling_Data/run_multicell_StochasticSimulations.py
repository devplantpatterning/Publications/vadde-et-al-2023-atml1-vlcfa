#!/usr/bin/env python

# This python script executes the ATML1 model in the tissue software and plot the results in histograms/time courses.
# At the moment, this script can run organism for different model files, and a single parameter set per model file. The results are generated and stored in a subfolder where this script is located.

############## Importing packages ###############
import matplotlib
matplotlib.use('Agg')

import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import pylab
import os
import shutil
import subprocess
import json
import pandas as pd
import scipy.io as sio
import generate_sim_files as gen # module to generate the simulation files

################################################

# Strings to modify
rootsim='../tissue/bin/simulator' # path in which the tissue executable simulator is located
#rootsim='/Users/pauformosa-jordan/Research/Codes/tissue_programs/Source_code_2/bin'
# Simulation parameters


#########################################################################################################################
# Parameters and strings to modify by the user
##############################################################################################################

# General script parameters. Set them to 1 to perform its associated task, otherwise, set them to 0.
matlab_analysis = 0
plot_data= 1
sims=1 # sims=1 will run the simulations, otherwise set it to 0


cost_positive = 0.5
cost_negative = 0.5
list_ics=[1] # list of initial conditionscd


dt = 0.001 #timestep for RK or HI method. Needed for timer data. Note that this does not change the timestep in the solver file, so please do both.
DrugTime = int(20/dt) #When the drug will start being produced

#Production Terms.
#betaA = 0.2 #atml13 basal production of mutant
betaA = 1.1 #WT basal production of ATML1
#betaA = 10 #ATML1OE basal production of  ATML1

betaV = 1.1 #Basal VLCFA Production
betaL = 10 #WT Basal LGO/Target Production
betaC = 0.0 #No Cafenstrole Production
#betaC = 300.0 #Max Cafenstrole Production

#Degredation Terms:
nuzero = 1.0 #Degredation rate for ATML1
nuone =  0.4 #Degredation rate for VLCFA-ATML1 Dimer (B in the model)
nutwo = 0.03 #Degredation rate for VLCFA
nuthree = 9 #Degredation rate for LGO/Target
nuC = 0 #Degredation rate for Cafenstrole

#Mass Action Terms
Kavplus = 15 #Rate at which 2A + 2V -> B occurs
Kavminus = 12 #Rate at which B -> 2A + 2V occurs


#Hill Function terms

#ATML1dimer promotes ATML1#z
betaa = 2.0 #Max production term for ATML1
Ka = 0.7  #Hill Constant for ATML1
na = 2 #Hill Coefficinet for ATML1

#ATML1dimer promotes VLCFA#
betav = 1.4 #Max production term for VLCFA
Kv = 0.6 #Hill Constant for VLCFA
nv = 1 # Hill Coefficinet for VLCFA

#ATML1dimer promotes LGO/Target#
betaell = 8.0 #Max production term for LGO
Kell = 1.4 #Hill Constant for LGO
nell = 7 #Hill Coefficinet for LGO

#Cafenstrole blocks basal production of VLCFA and prudction from dimerized ATML1)
Kc = 30 #Hill Constant for Cafenstrole
nc = 5 #Hill Coefficinet for Cafenstrole

#:GO thresholds
targetthres = 2.25 #Ht1
target2thres = 2.25/2 #Ht2


TimerRate = .45 #Timer basal production rate

thsphase = 2.5 #Threshold for synthesis (Hs)
TimerThres = 3.0 #Threshold for resetting the time (Hc)
TimerThres2 = 3.0 #Threshold for resetting the time (Hc), need both

# Strings to modify
string_sim ='' # Substring of the subfolder where the results are going to be stored

algorithm='hi' #  used algorthim (eg., rk, rk5.rk, hi and gi). Note this model has been prepared for the heun algorithm, ie., the 'hi' integrator.


model_name='ATML1-VLCFA_Stochastic'
model_filename="multicell_"+model_name+".model"
matlab_filename="script_ROC_sim_analysis.m"

string_pars='WT_Simulation_Caf{0}'.format(betaC) # # substring of the subfolder where the results are going to be stored

dataname='idealtemplate1' # name of the subfolder in InitFiles where there are the initial conditions. After running the simulation, a folder will be created in multicellTissue with this substring too, and the results are going to be allocated within this subdirectory.

initfilename='ideal_ic_Full' #Normal model file
#initfilename='ideal_ic_Full_atml13' #If you are using atml13 mutant, this zeros out all initial values

for ics in range(len(list_ics)) :
	modelpars_dict={'model_pars':{'dt': dt,
								  'betaA': betaA,
								  'betaV': betaV,
								  'betaC': betaC,
								  'betaL': betaL,
								  'nuzero': nuzero,
								  'nuone' : nuone,
								  'nutwo': nutwo,
								  'nuthree' : nuthree,
								  'nuC': nuC,
								  'Kavplus': Kavplus,
								  'Kavminus' : Kavminus,
								  'betaa' : betaa,
								  'Ka': Ka,
								  'na': na,
								  'betav' : betav,
								  'Kv': Kv,
								  'nv': nv,
								  'betaell' : betaell,
								  'Kell': Kell,
								  'nell': nell,
								  'betaV' : betaV,
		 					  	  'betav' : betav,
								  'Kc': Kc,
								  'nc': nc,
								  'DrugTime':DrugTime,
                                  'targetthres':targetthres,
                                  'target2thres':target2thres,
                                  'thsphase':thsphase,
								  'TimerRate':TimerRate,
								  'TimerThres':TimerThres,
								  'TimerThres2':TimerThres2}}

	modelfile_varying_pars_dict={'modelfile_varying_pars':{'dt':'dt',
								  'betaA': 'betaA',
								  'betaV': 'betaV',
								  'betaC': 'betaC',
								  'betaL': 'betaL',
								  'nuzero': 'nuzero',
								  'nuone' : 'nuone',
								  'nutwo': 'nutwo',
								  'nuthree' : 'nuthree',
								  'nuC': 'nuC',
								  'Kavplus': 'Kavplus',
								  'Kavminus' : 'Kavminus',
								  'betaa' : 'betaa',
								  'Ka': 'Ka',
								  'na': 'na',
								  'betav' : 'betav',
								  'Kv': 'Kv',
								  'nv': 'nv',
								  'betaell' : 'betaell',
								  'Kell': 'Kell',
								  'nell': 'nell',
								  'betaV' : 'betaV',
		 					  	  'betav' : 'betav',
								  'Kc': 'Kc',
								  'nc': 'nc',
								  'DrugTime':'DrugTime',
                                  'targetthres':'targetthres',
                                  'target2thres':'target2thres',
                                  'thsphase':'thsphase',
								  'TimerRate':'TimerRate',
								  'TimerThres':'TimerThres',
								  'TimerThres2':'TimerThres2'}}

	matlab_pars_dict={'matlab_pars':{'cost_positive':cost_positive,
								 'cost_negative':cost_negative}}

	matlabfile_varying_pars_dict={'matlabfile_varying_pars':{'cost_positive':'cplu',
													     'cost_negative':'cflu'}}

	strings_to_change_dict={'strings_to_change':{'dirini':'kplu',
												'dataname_out':'lalelilolu',
												'subfolder_out':'lele'}}

	info={'string_pars': string_pars}
	info.update(modelpars_dict)
	info.update(modelfile_varying_pars_dict)
	info.update(matlab_pars_dict)
	info.update(matlabfile_varying_pars_dict)

	info.update(strings_to_change_dict)
	with open('info.json','w') as f:
		json.dump(info, f)

	dirini=os.getcwd()  # storing the path in which the current script file is locate

	# creating the output folder in case it does not exist
	dataname_out='Results_'+dataname+'_'+initfilename
	try:
		os.stat(dataname_out)
	except:
		os.mkdir(dataname_out) # creating the directory

	strings_dict={'strings':{'model_name':model_name,
						'model_filename':model_filename,
						'dirini':dirini,
						'dataname_out': dataname_out,
						'matlab_filename':matlab_filename}}

	info.update(strings_dict)

	os.chdir(dirini)
	val_ic=list_ics[ics]

	initfile='InitFiles/'+dataname+'/'+initfilename+'_'+str(val_ic)+'.init'

	shutil.copyfile(initfile,dataname_out+'/'+initfilename+'_'+str(val_ic)+'.init')	# moving data init file into the output folder

	subfolder_out=string_sim+model_name+'_'+algorithm+'_pars_'+string_pars+"_ic_"+str(val_ic)
	newstrings={'subfolder_out':subfolder_out}
	info['strings'].update(newstrings)

	# simulation loop
	if (sims==1) :
		outfile=model_name+'_'+algorithm+'_num_out'
		dirout = dataname_out+'/'+subfolder_out
		try:
			os.stat(dirout)
		except:
			os.mkdir(dirout)
			#print('Created directory '+dirout)
		diroutvtk = dataname_out+'/'+subfolder_out+'/vtk'
		diroutvtk = 'vtk'

		try:
			os.stat(diroutvtk)
		except:
			os.mkdir(diroutvtk)

		# Generation of the model file
		gen.generate_model_file(info)

		#print(command0)
		modelfile='ModelFiles/'+model_filename
		initfile='InitFiles/'+dataname+'/'+initfilename+'_'+str(val_ic)+'.init'
		solverfile='SolverFiles/'+algorithm
		outfile='num_out'

		cmd=rootsim+' '+modelfile+' '+initfile+' '+solverfile+' > '+dirout+'/'+outfile  # command line for running organism
		print('Running '+cmd)
		subprocess.call(cmd,shell=True)
		shutil.copyfile(modelfile,dirout+'/'+model_filename)		# copying model file
		shutil.move('tissue.gdata',dirout+'/tissue.gdata')					    # moving output to the results subfolder
		if os.path.exists('neighbours.gdata'):
			shutil.move('neighbours.gdata',dirout+'/neighbours.gdata')

		shutil.copyfile(initfile,dirout+'/'+initfilename+'_'+str(val_ic)+'.init')				# copying init file to the results subfolder

		if os.path.exists(dirout+'/vtk'):					# creating a folder where the vtk files are going to  be moved.
			shutil.rmtree(dirout+'/vtk')
		shutil.move('vtk',dirout+'/vtk')
		shutil.move(dirout+'/tissue.gdata',
		dirout+'/tissue.gdata')				    # moving output to the results subfolder
				# moving vtk output to the results subfolder
	# plotting loop
	os.chdir(dirini)
	subfolder_out=string_sim+model_name+'_'+algorithm+'_pars_'+string_pars+"_ic_"+str(val_ic)
	dirout = dataname_out+'/'+subfolder_out
	shutil.copyfile('analysis_multicellTissue.py',dirout+'/analysis_multicellTissue.py')
	shutil.copyfile('find_maxima.py',dirout+'/find_maxima.py')
	shutil.copyfile('classifyTrueFalse.m',dirout+'/classifyTrueFalse.m')
	shutil.copyfile('info.json',dirout+'/info.json')

	if matlab_analysis==1:
		# generate matlab file
		gen.generate_matlab_file(info)
		shutil.copyfile(matlab_filename,dirout+'/'+matlab_filename)

	os.chdir(dirini+'/'+dirout)

	if plot_data==1 :
		subprocess.call("chmod 777 *.py",shell=True)
		if matlab_analysis==1 :
			subprocess.call("./find_maxima.py",shell=True)	# necessary for creating the dictionaries
			subprocess.call("matlab -nodisplay < "+matlab_filename+" > script_ROC_sim_analysis.log",shell=True)
			subprocess.call("./analysis_multicellTissue.py",shell=True)	# necessary for creating the dictionaries
		else :
			subprocess.call("./analysis_multicellTissue.py",shell=True)	# necessary for creating the dictionaries
