#!/usr/bin/env python

##############
#Importing packages
import os
import json
####################


def generate_model_file(info) :
    model_filename=info['strings']['model_filename']

    # Open the input template model file and read it
    fin = open("ModelFiles/pre_"+model_filename, "rt")
    config_text= fin.read()

    # Open the output configuration file to write to
    fout = open("ModelFiles/"+model_filename, "wt")
    #print(info['modelfile_varying_pars'])
    # Replace old strings with new strings
    for key in info['modelfile_varying_pars']:
            string_to_change=info['modelfile_varying_pars'][key]
            newvalue=info['model_pars'][key]
            config_text = config_text.replace(string_to_change,str(newvalue))
    fout.write(config_text)

    #close input and output files
    fin.close()
    fout.close()


def generate_matlab_file(info) :
    matlab_filename=info['strings']['matlab_filename']
    pre_matlab_filename='pre_'+matlab_filename

    # Open the input template configuration file and read it
    fin = open(pre_matlab_filename, "rt")
    config_text= fin.read()

    # Open the output configuration file to write to
    fout = open(matlab_filename, "wt")

    # Replace old strings with new strings
    for key in info['matlabfile_varying_pars']:
            par_to_change=info['matlabfile_varying_pars'][key]
            newvalue=info['matlab_pars'][key]
            config_text = config_text.replace(par_to_change,str(newvalue))
    for key in info['strings_to_change']:
            string_to_change=info['strings_to_change'][key]
            newstring=info['strings'][key]
            config_text = config_text.replace(string_to_change,newstring)


    fout.write(config_text)

    #close input and output files
    fin.close()
    fout.close()
