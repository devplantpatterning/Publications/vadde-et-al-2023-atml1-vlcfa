#!/usr/bin/env python


# This python script find the maxima (or peaks) of the different time courses, as explained in the Methods section in Meyer et al.

# Last modification the 10 October 2016.

# Contact: Henrik.Jonsson@slcu.cam.ac.uk, pau.formosajordan@slcu.cam.ac.uk.

###########################################################

# General script parameters and strings
auto=0 # Set it to 0 when no using auto

# Importing packages###############
import matplotlib
import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import pylab
import subprocess
from subprocess import call
from pylab import *
import os
import sys
import subprocess
import shutil
import pandas as pd
import pickle
import scipy
from scipy import stats
import itertools
import scipy.io as sio

#############################################

matplotlib.pyplot.ioff()

suggested_num_plotcells=1;

dirini=os.getcwd()

# Plotting related parameters
fz=28  # font size for the different plots
lw=2.0 # linewidth of the plots

## Color definitions ##
alphth=0.6;
colth='r';

lwth=4.0;
cmap=cm.jet;
cmap=cm.Set1;

diccolor={0:'deepskyblue',1:'crimson',2:'red',3:'lightsteelblue', 4:'green',5:'lavender'}

colorlist = [ (1,1,0,1.0),(0,0.7461,1.0000,1.0),(0.8594,0.0781,0.2344, 1.0)
            ,(132.0/255,112.0/255,1.0,1.0),(34.0/255,139.0/255,34.0/255,1.0),(1,239.0/255,213.0/255,1.0) ]
paumap = matplotlib.colors.ListedColormap(colorlist)

cmap=paumap;

color_small=(0.3906,0.5820,0.9258)
color_giant=(1.0000,0.2695,0)

filedat='tissue.gdata' # input file name

stepcoarse=16*5;

#############################################
x = pd.read_csv(filedat,sep=' ',skip_blank_lines=True,names=['None','Cell Number','time','A','B','C','D','step',
                             'atml1','target','target_marker','timer','timer_marker','div_marker',
                              's_marker','counter_post_s','ploidy','E','F',
                              'clone_id','X14','cell_id','par_id','vlcfa','aavlcfa',
                              'Cafenstrole','totatml1','Cell Number 2','area','Num Verticies'])
num_steps= max(x.step);
init_step= min(x.step);


fin_time= max(x.time);
init_time= min(x.time);

final_cell_ids=x[x.step==num_steps].cell_id;
num_final_cells=len(final_cell_ids)
max_ploidy=x['ploidy'].max();
min_ploidy=x[x.step==num_steps]['ploidy'].min();

time_points=x['time'].unique()
step_points=x['step'].unique()

dt=time_points[1]-time_points[0]


# storing the print_step_increment
preprinted_steps=x.step.sort_values()
printed_steps=preprinted_steps.unique()
print_step_increment=printed_steps[2]-printed_steps[1]
printed_times=x.time.unique()
print_time_increment=printed_times[2]-printed_times[1]

xcoarse=x[x.step.isin(printed_steps[0::stepcoarse])]

# Creating the tissue_classes_dict. This creates two different classes: giant and nongiant cells.

tissue_classes_dict={'classes':['giant','nongiant'],'vars':['totatml1','target'],'giant':{'cell_ids':[],'numcells':[],'totatml1':{},'target':{}},'nongiant':{'cell_ids':[],'numcells':[],'totatml1':{},'target':{}}};
cells_id_4C=x[x.ploidy==1].cell_id.unique()
for jj in range(0,len(cells_id_4C),1) :
	last_4C_step=x[(x.cell_id==cells_id_4C[jj]) & (x.ploidy==1)].step.max() # max step for a certain 4C cell
	next_4C_step=last_4C_step+print_step_increment
	# here we are trying to find whether 4C cells continue existing
	# after losing 4C identity. if cell_id_next4C is empty, it means cell has divided.
	cell_id_next4C=x[(x.cell_id==cells_id_4C[jj]) & (x.step==next_4C_step)].cell_id
	nongiant_class=cell_id_next4C.empty # if there is no continuity, such 4C will divide, and cell_id_next4C is true.
	if nongiant_class==True and last_4C_step<num_steps :              # we consider a nongiant cell if it does not have continuity in the lineage (ie it divides) and it is not at the final time point
		tissue_classes_dict['nongiant']['cell_ids'].append(cells_id_4C[jj])
	elif nongiant_class==False :
		tissue_classes_dict['giant']['cell_ids'].append(cells_id_4C[jj])

# Length of number of cells in each class
for kk in range(0,len(tissue_classes_dict['classes']),1) :
	classname=tissue_classes_dict['classes'][kk]
	num_cells=len(tissue_classes_dict[classname]['cell_ids'])
	tissue_classes_dict[classname]['numcells']=num_cells


# Find maxima in each class (giant and nongiant)
for ii in range(0,2,1) :
	for kk in range(0,len(tissue_classes_dict['classes']),1) :
		classname=tissue_classes_dict['classes'][kk]
		num_cells=tissue_classes_dict[classname]['numcells']
		varname=tissue_classes_dict['vars'][ii]
		#print(classname, varname)

		list_maxs4C=[]
		list_maxs2C=[]
		list_maxs4C_coarse=[]
		list_maxs2C_coarse=[]
		for jj in range(0,num_cells,1) :
			selection=(x.cell_id==tissue_classes_dict[classname]['cell_ids'][jj]) # list of booleans for x. picking the right cell id's.
			selectioncoarse=(xcoarse.cell_id==tissue_classes_dict[classname]['cell_ids'][jj]) # list of booleans for xcoarse. picking the right cell id's.

			# Find the maxima in traces
			# First we need to get rid of empty traces ; ie a small cell lineage might start at 4C in the simulation, and not have 2C time points
			trace4C=x[(selection) & (x.ploidy==1)][varname]
			if not trace4C.empty :
				max4C=trace4C.max()
				list_maxs4C.append(max4C)


			trace2C=x[(selection) & (x.ploidy==0)][varname]
			if not trace2C.empty :
				max2C=trace2C.max()
				list_maxs2C.append(max2C)


			tracecoarse4C=xcoarse[(selectioncoarse) & (xcoarse.ploidy==1)][varname]
			if not tracecoarse4C.empty :
				max4C_coarse=tracecoarse4C.max()
				list_maxs4C_coarse.append(max4C_coarse)


			tracecoarse2C=xcoarse[(selectioncoarse) & (xcoarse.ploidy==0)][varname]
			if not tracecoarse2C.empty :
				max2C_coarse=tracecoarse2C.max()
				list_maxs2C_coarse.append(max2C_coarse)



		tissue_classes_dict[classname][varname].update({'maxs4C':list_maxs4C})
		tissue_classes_dict[classname][varname].update({'maxs2C':list_maxs2C})
		tissue_classes_dict[classname][varname].update({'maxs4C_coarse':list_maxs4C_coarse})
		tissue_classes_dict[classname][varname].update({'maxs2C_coarse':list_maxs2C_coarse})
tissue_classes_dict.update({'datapath':dirini})

# Export dictionary tissue_classes_dict
scipy.io.savemat(dirini+'/'+'out.mat', tissue_classes_dict)
