#!/usr/bin/env python

# This python script plots different data and performs data analysis on that.

###########################################################

v14='vlcfa'
v15='aavlcfa'
v20='ZZ'
#Dimer model
#v14='ATML1-VLCFA'
#v15='ATML1-VLCFA'
#

# General script parameters and strings
colorline_ploidy=1
plothistoclasses=1
plot_matlab_threshold=0
plot_tc_coarse_cellgroups=1

auto=0
#cost_positive=0.5
#cost_negative=0.5
num_separated_plotted_cells=5; # number of plots of giant cells (non lineage plot)
num_nong_separated_plotted_cells=5; # number of plots of non-giant cells (non lineage plot)
suggested_num_plotcells=1; # number cells per time course plot [note this won't affect families, nongiant and divgianttree]

# Importing packages###############
import matplotlib
matplotlib.use('Agg')

import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import pylab
import subprocess
from subprocess import call
from pylab import *
import os
import sys
import subprocess
import shutil
import pandas as pd
import json
import scipy
from scipy import stats
import scipy.io as sio

#############################################

matplotlib.pyplot.ioff()

dirini=os.getcwd()

# Plotting related parameters
fz=28  # font size for the different plots
lw=2.0 # linewidth of the plots

## Color definitions ##
alphth=0.6;
colth='r';

lwth=4.0;
cmap=cm.jet;
cmap=cm.Set1;

#diccolor={0:'deepskyblue',1:'crimson',2:'red',3:'lightsteelblue', 4:'green',5:'lavender'}
diccolor = {0:'#b35806',1:'#f1a340',2:'#fee0b6',3:'#d8daeb', 4:'#998ec3',5:'#542788'}
# check last colour
colorlist = [ (1,1,0,1.0),(0,0.7461,1.0000,1.0),(0.8594,0.0781,0.2344, 1.0)
			,(132.0/255,112.0/255,1.0,1.0),(34.0/255,139.0/255,34.0/255,1.0),(1,239.0/255,213.0/255,1.0),(239.0/255,239.0/255,213.0/255,1.0) ]
paumap = matplotlib.colors.ListedColormap(colorlist)

cmap=paumap;

color_small=(0.3906,0.5820,0.9258)
color_giant=(1.0000,0.2695,0)

filedat='tissue.gdata' # input file name
plottccoarse=1;
stepcoarse=16;

stepcoarse=16*5;

xlabels={'atml1':'$[ATML_i]$','target':'$[LGO_i]$','target_marker':'$[LGO MARKER_i]$','timer':'$[TIMER_i]$','timer_marker':'$[Timer MARKER_i]$','div_marker':'$[DIV MARKER _i]$',
's_marker':'$[S MARKER_i]$','counter_post_s':'$[COUNT POST S_i]$','ploidy':'$PLOIDY_i$',
'YYY': '$[YYY]$' ,'XXX': '$[XXX]$','area':'Area','vlcfa':'$[VLCFA_i]$','aavlcfa':'$[Dim. ATML1_i]$','totatml1':'$[ATML1_i]+[Dim. ATML1_i]$'} # labels used in the x axis
vars_histoploidies=['atml1','target','totatml1','area'];
#######################################################
with open("info.json", "r") as f:
    info= json.load(f)

targetthres=info['model_pars']['targetthres']; # target levels above which mitosis is prevented
deg=info['model_pars']['nuthree'];
Vm=info['model_pars']['betaL'];

#cost_positive=info['matlab_pars']['cost_positive']
#cost_negative=info['matlab_pars']['cost_negative']
cost_positive = 0.5
cost_negative = 0.5
#############################################
x = pd.read_csv(filedat,sep=' ',skip_blank_lines=True,names=['None','Cell Number','time','A','B','C','D','step',
                             'atml1','target','target_marker','timer','timer_marker','div_marker',
                              's_marker','counter_post_s','ploidy','E','F',
                              'clone_id','X14','cell_id','par_id','vlcfa','aavlcfa',
                              'Cafenstrole','totatml1','Cell Number 2','area','Num Verticies'])
num_steps= max(x.step);
init_step= min(x.step);

fin_time= max(x.time);
init_time= min(x.time);

final_cell_ids=x[x.step==num_steps].cell_id;
num_final_cells=len(final_cell_ids)
max_ploidy=x['ploidy'].max();
min_ploidy=x[x.step==num_steps]['ploidy'].min();

time_points=x['time'].unique()
step_points=x['step'].unique()

dt=time_points[1]-time_points[0]
print('dt=',dt)
print('max ploidy=',max_ploidy)

atml1ymax=max(x['atml1'])*1.05;
targymax=max(x['target'])*1.05;

# storing the print_step_increment
preprinted_steps=x.step.sort_values()
printed_steps=preprinted_steps.unique()
print_step_increment=printed_steps[2]-printed_steps[1]
printed_times=x.time.unique()
print_time_increment=printed_times[2]-printed_times[1]

xcoarse=x[x.step.isin(printed_steps[0::stepcoarse])]


# Indices of cells that are going to be giant and nongiant. This will be important for building dictionaries.
giantcellsploid = x[x.ploidy>1].cell_id.unique();
nongiant_finalcells=x[(x.step==num_steps) & (x.ploidy==0)].cell_id.unique(); # NOTE WE PUT <1 to AVOID cells being 4C at the final time point

final_giantcells = x[(x.step==num_steps) & (x.ploidy>1)].cell_id.unique();
dividing_giantcells=np.setxor1d(final_giantcells, giantcellsploid)

fields_tc=['atml1','target','area','timer','target_marker','div_marker']

fields_tc_coarse=['atml1','target','timer','ploidy','totatml1']
fields_tc=['atml1','target','area','timer','target_marker','div_marker','vlcfa','aavlcfa','totatml1']

cellselection=giantcellsploid;
###


filedat='outROC.mat'
fname_matlab='ROCs_cP'+str(cost_positive)+'_cN'+str(cost_negative)+'/'+filedat
if (plot_matlab_threshold==1 & os.path.isfile(fname_matlab)==1) :
	we_can_matlab=1
	print('Loading matlab analysis outcome for plotting')
	dd=sio.loadmat(fname_matlab)
	var='atml1';

	dataset='maxs4C';
	allauc_4C=dd['outdata'][dataset][0][0][var][0][0]['allauc'][0][0][0][0]
	allths_4C=dd['outdata'][dataset][0][0][var][0][0]['allths'][0][0][0][0]
	thsmean_4C=dd['outdata'][dataset][0][0][var][0][0]['thsmean'][0][0][0][0]
	thsstd_4C=dd['outdata'][dataset][0][0][var][0][0]['thsstd'][0][0][0][0]
	thstop_4C=thsmean_4C+thsstd_4C
	thsbot_4C=thsmean_4C-thsstd_4C

	dataset='maxs2C';
	allauc_2C=dd['outdata'][dataset][0][0][var][0][0]['allauc'][0][0][0][0]
	allths_2C=dd['outdata'][dataset][0][0][var][0][0]['allths'][0][0][0][0]

	print(dd['outdata'][dataset][0][0][var][0][0])

	dataset='maxs4C_coarse';
	allauc_4C_coarse=dd['outdata'][dataset][0][0][var][0][0]['allauc'][0][0][0][0]
	allths_4C_coarse=dd['outdata'][dataset][0][0][var][0][0]['allths'][0][0][0][0]
	thsmean_4C_coarse=dd['outdata'][dataset][0][0][var][0][0]['thsmean'][0][0][0][0]
	thsstd_4C_coarse=dd['outdata'][dataset][0][0][var][0][0]['thsstd'][0][0][0][0]
	thstop_4C_coarse=thsmean_4C_coarse+thsstd_4C_coarse
	thsbot_4C_coarse=thsmean_4C_coarse-thsstd_4C_coarse

	dataset='maxs2C_coarse';
	allauc_2C_coarse=dd['outdata'][dataset][0][0][var][0][0]['allauc'][0][0][0][0]
	allths_2C_coarse=dd['outdata'][dataset][0][0][var][0][0]['allths'][0][0][0][0]


	print('matlab worked')
else:
	we_can_matlab=0


#####################################################################
print('Building cell dicctionaries')

# BUILDING PARENTAL LINEAGES DICTIONARY.
# This is a dictionary that contains all the ids of the final cell array, and an associated vector that says the different cell ids of its parents
tissue_parental_dict={}
for jj in range(0,num_final_cells,1) :
	index_cell_id=final_cell_ids.values[jj]
	lin_index_cell_id=index_cell_id
	celltrack=[]
	cellpar = -1
	while (cellpar!=0) :
		cellpar=x[x.cell_id==index_cell_id].par_id.unique()
		if cellpar[0]!=0 :
			celltrack.append(cellpar[0])
		index_cell_id=cellpar[0]
	celltrack=sort(celltrack)
	parental_dic={(lin_index_cell_id) : celltrack}
	tissue_parental_dict.update(parental_dic)

print('Building lineages')

tissue_lin_dict={} # this dictionary is basically the same as the parental one, but also having the final cell of the lineage
for jj in range(0,num_final_cells,1) :
	index_cell_id=final_cell_ids.values[jj]
	lin=tissue_parental_dict[index_cell_id]
	lin=np.append(lin,index_cell_id)
	tissue_lin_dict[index_cell_id]=lin

num_nongiant_fin=len(nongiant_finalcells) # Number of nongiant cells at the end of the simulation
num_nongiant_fin=min(num_nongiant_fin,suggested_num_plotcells) # currently suggested_num_plotcells=1, so it will produce 1 lineage.

print('Building lists')

nongiant_lin_list=[] # dictionary for building the groups dictionary.
#for jj in nongiant_finalcells :
ini_nongiant=0
for kk in range(ini_nongiant,num_nongiant_fin,1) :
	jj=nongiant_finalcells[kk]
	lincells=tissue_lin_dict[jj] # gets the lineage of a particular cell.
	nongiant_lin_list=np.concatenate((lincells,nongiant_lin_list), axis=0)

nongiant_lin_list=np.unique(nongiant_lin_list)

# Note creating family lists takes more time
print('Building family lists')
tissue_family_list=[]
numfamilies=3

#for ii in range(0,num_final_cells,1) :
for ii in range(0,numfamilies,1) :
	jj=final_cell_ids.values[ii]
	family=np.array(tissue_lin_dict[jj])
	for kk in range(0,num_final_cells,1) :
		aproj=tissue_lin_dict[jj]
		bproj=tissue_lin_dict[final_cell_ids.values[kk]]
		c=np.intersect1d(aproj,bproj)
		if c.size>0 :
			family=np.concatenate((bproj,family), axis=0)
	family=np.unique(family)
	family=sort(family)
	if family.size>=1 :
		fam=tuple(family)
		tissue_family_list.append(fam)
print(tissue_family_list)
families=np.unique(tissue_family_list)
#print(str(families))
familyind=1;
if isinstance(families[familyind],integer)==1 :
	selectedfamily=np.array([families[familyind]])
else :
	selectedfamily=np.array(families[familyind])

# making a list of final indices for a dividing giant cell for plotting
num_dividing_giantcells=len(dividing_giantcells)
if num_dividing_giantcells>0 :
	kk=0
	divcell_index=dividing_giantcells[kk]
	# starting a list of indices
	divgiant_final_indexs=[]
	for jj in range(0,num_final_cells,1) :
		index_cell_id=final_cell_ids.values[jj]
		if divcell_index in tissue_lin_dict[index_cell_id] :
			divgiant_final_indexs=np.union1d(divgiant_final_indexs,tissue_lin_dict[index_cell_id])
	#print(divgiant_final_indexs)
	#divgiant_final_indexs=np.unique(np.array(divgiant_final_indexs))
	#divgiant_final_indexs=np.array(divgiant_final_indexs)
##############
# tissue_cellgroups_dict: Dictionary defining the groups of interest. Contains'classes' subdic, with the strings of the groups, and subdics of the groups per se, with their ids and number of cells.
# The giantcellsploid dictionary contains the ids of the cells that become giant. Not the full lineage though.
print('Building cell groups')

tissue_cellgroups_dict={};
#if giantcellsploid.size>0 :
#	subdic={'giant':{'cell_ids': giantcellsploid ,'numcells': giantcellsploid.size}}
#	tissue_cellgroups_dict.update(subdic)

if final_giantcells.size>0 :
	subdic={'giant':{'cell_ids': final_giantcells ,'numcells': final_giantcells.size}}
	tissue_cellgroups_dict.update(subdic)

if dividing_giantcells.size>0 :
	subdic={'giantdiv':{'cell_ids': dividing_giantcells ,'numcells': dividing_giantcells.size}}
	tissue_cellgroups_dict.update(subdic)
	subdic={'giantdivtree':{'cell_ids': divgiant_final_indexs,'numcells': divgiant_final_indexs.size}}
	tissue_cellgroups_dict.update(subdic)

if nongiant_lin_list.size>0 :
	subdic={'nongiant_lin_list':{'cell_ids': nongiant_lin_list,'numcells': nongiant_lin_list.size}}
	tissue_cellgroups_dict.update(subdic)

if selectedfamily.size>1 :
	subdic={'family':{'cell_ids':selectedfamily,'numcells':selectedfamily.size}}
	tissue_cellgroups_dict.update(subdic)

subdic={'classes': list(tissue_cellgroups_dict.keys())}
print(tissue_cellgroups_dict)
tissue_cellgroups_dict.update(subdic)

#########################################################################
print('Cell dictionaries completed')

print('Plotting outcomes')

dirout = 'tc_cellgroups'
try:
	os.stat(dirout)
except:
	os.mkdir(dirout)
fig_size=2
params ={'axes.labelsize': fz,'font.size': fz,'xtick.labelsize': fz,'ytick.labelsize': fz}
pylab.rcParams.update(params)
for ii in range(0,len(fields_tc),1) :
	var=fields_tc[ii]
	xmin=0;xmax=max(x.time)*1.01;
	ymin=min(0,min(x[var])*1.05);ymax=max(x[var])*1.05;
	scale='linear'
	if fields_tc[ii]=='area' :
		scale='log'
	for kk in range(0,len(tissue_cellgroups_dict['classes']),1):
		cellselectiongroup=tissue_cellgroups_dict['classes'][kk]
		cellselection=tissue_cellgroups_dict[cellselectiongroup]['cell_ids']
		#print(cellselectiongroup,cellselection.size)
		num_plotcells=min(cellselection.size,suggested_num_plotcells) # Number of cells per plot [note this won't affect families, nongiant and divgianttree]
		class_num_of_plots=1 # Number of plots for each tc class.
		if cellselectiongroup=='giant' :
			class_num_of_plots=min(num_separated_plotted_cells,len(giantcellsploid))
			#class_num_of_plots=len(giantcellsploid)
		elif cellselectiongroup=='nongiant_lin_list' :
			class_num_of_plots=min(num_nong_separated_plotted_cells,len(nongiant_finalcells))
		for ik in range(0,class_num_of_plots,1) :
			ini_cell_to_plot=ik
			fig, (ax1) =plt.subplots(1,1)
			#pylab.figure()
			plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)
			if ((cellselectiongroup=='nongiant_lin_list') | (cellselectiongroup=='family') | (cellselectiongroup=='giantdivtree')) :
				subcellselection=cellselection
			else :
				subcellselection=cellselection[ini_cell_to_plot:num_plotcells+ini_cell_to_plot];
			for index_cell_id in subcellselection :
				if colorline_ploidy==0 : # NOTE WE DON'T CONCATENATE for the colors..
					xs=x[x.cell_id==index_cell_id].time.values
					ys=x[x.cell_id==index_cell_id][var].values
					zs=x[x.cell_id==index_cell_id].ploidy.values
					if len(xs)>0 :
						if cellselectiongroup!='giant' and min(xs)>0 :
							cell_par_id=cell_par_id[0]
							max_time_par=x[x.cell_id==cell_par_id]['time'].max()
							y_max_time_par=x[x.cell_id==cell_par_id][x.time==max_time_par][var].values
							z_max_time_par=x[x.cell_id==cell_par_id][x.time==max_time_par]['ploidy'].values
							xs=np.append(max_time_par,xs)  # concatenating
							ys=np.append(y_max_time_par,ys)  # concatenating
							zs=np.append(z_max_time_par,zs)  # concatenating
					plt.plot(xs,ys,'-',linewidth=lw)
				else :
					ploidies_cell=x[x.cell_id==index_cell_id].ploidy.unique()
					for kz in ploidies_cell :
						xs=x[(x.cell_id==index_cell_id) & (x.ploidy==kz)].time.values
						ys=x[(x.cell_id==index_cell_id) & (x.ploidy==kz)][var].values
						plt.plot(xs,ys,'-',linewidth=lw,color=colorlist[kz])
				plt.xlabel('Time [AU]')
				plt.ylabel(xlabels[var])
			if var=='atml1' :
				#ymax=2.3
				ymax=atml1ymax
				if (plot_matlab_threshold==1 & we_can_matlab==1) :
					xf = np.arange(0.0, 200, 1)
					plt.axhline(y=thsmean_4C,linewidth=lwth, color='r',linestyle='--',alpha=alphth)
					y1=thsbot_4C
					y2=thstop_4C
					yy1 = np.zeros(len(xf))+thsbot_4C
					yy2 = np.zeros(len(xf))+thstop_4C
					ax1.fill_between(xf, yy1, yy2, facecolor='red',alpha=0.2)


			elif var=='target' :
				plt.axhline(y=targetthres,linewidth=lwth, color=colth,linestyle='--',alpha=alphth)
				plt.axhline(y=targetthres/2.0,linewidth=lwth, color=colth,linestyle='-',alpha=alphth)

			elif var=='timer' :
				plt.axhline(y=3,linewidth=lwth, color='red',linestyle='--',alpha=alphth)
				plt.axhline(y=2,linewidth=lwth, color='blue',linestyle='--',alpha=alphth)

			#plt.yscale(scale, nonposy='clip');
			plt.yscale(scale)

			#plt.axis([xmin, xmax,ymin, ymax])
			plt.tight_layout()
			fileout='_'+var+'_'+str(num_plotcells)+cellselectiongroup+'_cell_'+str(ik)
			fname="tc"+fileout+".pdf"
			fullfname=dirout+'/'+fname
			plt.savefig(fullfname, format=None)
			plt.close()


if plot_tc_coarse_cellgroups==1 :
	dirout = 'tc_coarse_cellgroups'
	try:
		os.stat(dirout)
	except:
		os.mkdir(dirout)
	for ii in range(0,len(fields_tc_coarse),1) :
		var=fields_tc_coarse[ii]
		xmin=0;xmax=max(xcoarse.time)*1.01;
		ymin=min(0,min(xcoarse[var])*1.05);ymax=max(xcoarse[var])*1.05;
		if var=='atml1' :
			ymax=2.3;
		for kk in range(0,len(tissue_cellgroups_dict['classes']),1)	:
			cellselectiongroup=tissue_cellgroups_dict['classes'][kk]
			cellselection=tissue_cellgroups_dict[cellselectiongroup]['cell_ids']
			num_plotcells=min(len(cellselection),suggested_num_plotcells)

			class_num_of_plots=1
			if cellselectiongroup=='giant' :
				class_num_of_plots=min(num_separated_plotted_cells,len(giantcellsploid))
				#class_num_of_plots=len(giantcellsploid)

			elif cellselectiongroup=='nongiant_lin_list' :
				class_num_of_plots=min(num_nong_separated_plotted_cells,len(nongiant_finalcells))
			for ik in range(0,class_num_of_plots,1) :
				ini_cell_to_plot=ik

				fig, (ax1) =plt.subplots(1,1)

				plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)
				if ((cellselectiongroup=='nongiant_lin_list') | (cellselectiongroup=='family')) :
					#subcellselection=-np.sort(-cellselection)
					subcellselection=np.sort(cellselection)
				else :
					subcellselection=cellselection[ini_cell_to_plot:num_plotcells+ini_cell_to_plot];
				#print('subcells',subcellselection)

				for index_cell_id in subcellselection :
					xs=xcoarse[xcoarse.cell_id==index_cell_id].time.values
					ys=xcoarse[xcoarse.cell_id==index_cell_id][var].values
					zs=xcoarse[xcoarse.cell_id==index_cell_id]['ploidy'].values
					# print("former zs",str(zs),"cell_id",str(index_cell_id))
					if len(xs)>0 :
					#if len(xs)>0 :
						if cellselectiongroup!='giant' and min(xs)>0 :
							cell_par_id=xcoarse[xcoarse.cell_id==index_cell_id]['par_id'].unique()
							cell_par_id=cell_par_id[0] # looking for the parent

							max_time_par=xcoarse[xcoarse.cell_id==cell_par_id]['time'].max() # maximal time of a parental cell
							y_max_time_par=xcoarse[(xcoarse.cell_id==cell_par_id) & (xcoarse.time==max_time_par)][var].values
							z_max_time_par=xcoarse[(xcoarse.cell_id==cell_par_id) & (xcoarse.time==max_time_par)]['ploidy'].values
							xs=np.append(max_time_par,xs)    # concatenating
							ys=np.append(y_max_time_par,ys)  # concatenating
							zs=np.append(z_max_time_par,zs)  # concatenating
						#Pending to check why is necessary this nan
						if not(np.isnan(np.sum(xs))) :
							xsc=xs
							ysc=ys
							plt.scatter(xsc,ysc, c=zs,lw=0.2, cmap=cmap,s=100,zorder=1000, vmin=0, vmax=5) # alpha=0.7

							if cellselectiongroup=='nongiant_lin_list' :
								plt.plot(xs,ys,'-',linewidth=lw,color=color_small)
							elif cellselectiongroup=='giant' :
								plt.plot(xs,ys,'-',linewidth=lw,color=color_giant)
							else :
								plt.plot(xs,ys,'-',linewidth=lw,color=color_small)

				if var=='atml1' :
					ymax=atml1ymax


					if (plot_matlab_threshold==1 & we_can_matlab==1):
						xf = np.arange(0.0, 200, 1)
						plt.axhline(y=thsmean_4C_coarse,linewidth=lwth, color='r',linestyle='--',alpha=alphth)
						y1=thsbot_4C
						y2=thstop_4C
						yy1 = np.zeros(len(xf))+thsbot_4C_coarse
						yy2 = np.zeros(len(xf))+thstop_4C_coarse
						ax1.fill_between(xf, yy1, yy2, facecolor='red',alpha=0.2)


				#print('zs',zs)
				plt.xlabel('Time [AU]')
				plt.ylabel(xlabels[var])

				plt.axis([xmin, xmax,ymin, ymax])
				fileout='_'+var+'_'+str(num_plotcells)+cellselectiongroup
				fname="tc_coarse"+fileout+'_cell_'+str(ik)+".pdf"
				fullfname=dirout+'/'+fname
				plt.savefig(fullfname, format=None)
				plt.close()


# Creating the tissue_classes_dict. This creates two different classes: giant and nongiant cells.

tissue_classes_dict={'classes':['giant','nongiant'],'vars':['atml1','target'],'giant':{'cell_ids':[],'numcells':[],'atml1':{},'target':{}},'nongiant':{'cell_ids':[],'numcells':[],'atml1':{},'target':{}}};
cells_id_4C=x[x.ploidy==1].cell_id.unique()
for jj in range(0,len(cells_id_4C),1) :
	last_4C_step=x[(x.cell_id==cells_id_4C[jj]) & (x.ploidy==1)].step.max() # max step for a certain 4C cell
	next_4C_step=last_4C_step+print_step_increment
	# here we are trying to find whether 4C cells continue existing
	# after losing 4C identity. if cell_id_next4C is empty, it means cell has divided.
	cell_id_next4C=x[(x.cell_id==cells_id_4C[jj]) & (x.step==next_4C_step)].cell_id
	nongiant_class=cell_id_next4C.empty # if there is no continuity, such 4C will divide, and cell_id_next4C is true.
	if nongiant_class==True and last_4C_step<num_steps :              # we consider a nongiant cell if it does not have continuity in the lineage (ie it divides) and it is not at the final time point
		tissue_classes_dict['nongiant']['cell_ids'].append(cells_id_4C[jj])
		#print('non')
	elif nongiant_class==False :
		tissue_classes_dict['giant']['cell_ids'].append(cells_id_4C[jj])

# Length of number of cells in each class
for kk in range(0,len(tissue_classes_dict['classes']),1) :
	classname=tissue_classes_dict['classes'][kk]
	num_cells=len(tissue_classes_dict[classname]['cell_ids'])
	tissue_classes_dict[classname]['numcells']=num_cells


# Find maxima in each class (giant and nongiant)
for ii in range(0,2,1) :
	for kk in range(0,len(tissue_classes_dict['classes']),1) :
		classname=tissue_classes_dict['classes'][kk]
		num_cells=tissue_classes_dict[classname]['numcells']
		varname=tissue_classes_dict['vars'][ii]
		#print(classname, varname)

		list_maxs4C=[]
		list_maxs2C=[]
		list_maxs4C_coarse=[]
		list_maxs2C_coarse=[]
		for jj in range(0,num_cells,1) :
			selection=(x.cell_id==tissue_classes_dict[classname]['cell_ids'][jj]) # list of booleans for x. picking the right cell id's.
			selectioncoarse=(xcoarse.cell_id==tissue_classes_dict[classname]['cell_ids'][jj]) # list of booleans for xcoarse. picking the right cell id's.

			# Find the maxima in traces
			# First we need to get rid of empty traces ; ie a small cell lineage might start at 4C in the simulation, and not have 2C time points
			trace4C=x[(selection) & (x.ploidy==1)][varname]
			if not trace4C.empty :
				max4C=trace4C.max()
				list_maxs4C.append(max4C)


			trace2C=x[(selection) & (x.ploidy==0)][varname]
			if not trace2C.empty :
				max2C=trace2C.max()
				list_maxs2C.append(max2C)
				#if np.isnan(max2C)==1 :
				#	print('Nan max2C')

			tracecoarse4C=xcoarse[(selectioncoarse) & (xcoarse.ploidy==1)][varname]
			if not tracecoarse4C.empty :
				max4C_coarse=tracecoarse4C.max()
				list_maxs4C_coarse.append(max4C_coarse)
				#if np.isnan(max4C_coarse)==1 :
				# print('Nan max4C_coarse')

			tracecoarse2C=xcoarse[(selectioncoarse) & (xcoarse.ploidy==0)][varname]
			if not tracecoarse2C.empty :
				max2C_coarse=tracecoarse2C.max()
				list_maxs2C_coarse.append(max2C_coarse)
				#if np.isnan(max2C_coarse)==1 :
				# print('Nan max2C_coarse')



		tissue_classes_dict[classname][varname].update({'maxs4C':list_maxs4C})
		tissue_classes_dict[classname][varname].update({'maxs2C':list_maxs2C})
		tissue_classes_dict[classname][varname].update({'maxs4C_coarse':list_maxs4C_coarse})
		tissue_classes_dict[classname][varname].update({'maxs2C_coarse':list_maxs2C_coarse})
print('yay')

tissue_classes_dict.update({'datapath':dirini})


# Export dictionary tissue_classes_dict
# note this export produces a warning
scipy.io.savemat(dirini+'/'+'out.mat', tissue_classes_dict)

type_of_concentrations=['maxs4C','maxs2C','maxs4C_coarse','maxs2C_coarse']
ymax=num_final_cells/4;

ymin=0;
nbins=40;
we_can_matlab=0

if plothistoclasses==1 :
	dirout='plots_classes'
	try:
		os.stat(dirout)
	except:
		os.mkdir(dirout)

	sxmax=max(x['target'])*1.05;
	symax=max(x['atml1'])*1.05;

	xmin=0;
	for jj in range(0,len(type_of_concentrations),1) :
		type_conc=type_of_concentrations[jj]
		ymin=0;
		fig, (ax1) =plt.subplots(1,1)

		#pylab.figure()
		plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)
		xsample=tissue_classes_dict['nongiant']['target'][type_conc]
		ysample=tissue_classes_dict['nongiant']['atml1'][type_conc]
		if len(xsample)>0 :
			plt.scatter(xsample,ysample, lw=0.8, c=color_small,s=100,alpha=0.7,zorder=1000, vmin=0, vmax=5)
		x2sample=tissue_classes_dict['giant']['target'][type_conc]
		y2sample=tissue_classes_dict['giant']['atml1'][type_conc]
		if len(x2sample)>0 :
			plt.scatter(x2sample,y2sample, lw=0.8, c=color_giant,s=100,alpha=0.7,zorder=1000, vmin=0, vmax=5)

		plt.axvline(x=targetthres,linewidth=lwth, color='red',linestyle='--',alpha=alphth)
		#xf = np.arange(0.0, symax, 0.1)
		if we_can_matlab==1 :
			if jj>1 : # rather for the coarse grained data. Plotting the threshold computed by me.

				y1=thsbot_4C
				y2=thstop_4C
				yy1 = np.zeros(len(xf))+thsbot_4C_coarse
				yy2 = np.zeros(len(xf))+thstop_4C_coarse
				plt.axhline(y=thsmean_4C_coarse,linewidth=lwth, color='r',linestyle='--',alpha=alphth)

			else :

				y1=thsbot_4C
				y2=thstop_4C
				yy1 = np.zeros(len(xf))+thsbot_4C
				yy2 = np.zeros(len(xf))+thstop_4C
				plt.axhline(y=thsmean_4C,linewidth=lwth, color='r',linestyle='--',alpha=alphth)


			ax1.fill_between(xf, yy1, yy2, facecolor='red',alpha=0.2)

		plt.yticks(np.arange(0,2.9,0.5),(0,0.5,1,1.5,2,2.5))

		plt.xticks(np.arange(0,0.9,0.2),(0,0.2,0.4,0.6,0.8))
		plt.xlabel(xlabels['target'])
		plt.ylabel(xlabels['atml1'])
		plt.axis([xmin, sxmax,ymin, symax])
		fileout='_atml1_vs_target_'+type_conc
		fname="scatt"+fileout+".pdf"
		fullfname=dirout+'/'+fname
		plt.savefig(fullfname, format=None)
		plt.close()


c0=x[(x.step==num_steps) & (x.ploidy==0)].shape[0]
c1=x[(x.step==num_steps) & (x.ploidy==1)].shape[0]
c2=x[(x.step==num_steps) & (x.ploidy==2)].shape[0]
c3=x[(x.step==num_steps) & (x.ploidy==3)].shape[0]
c4=x[(x.step==num_steps) & (x.ploidy==4)].shape[0]
c5=x[(x.step==num_steps) & (x.ploidy==5)].shape[0]

c1x=x[(x.step==num_steps) & (x.ploidy>=1)].shape[0] # 4C cells and larger ploidies
c2x=x[(x.step==num_steps) & (x.ploidy>=2)].shape[0] # 8C cells and larger ploidies
c3x=x[(x.step==num_steps) & (x.ploidy>=3)].shape[0] # 16C cells and larger ploidies
c4x=x[(x.step==num_steps) & (x.ploidy>=4)].shape[0] # 32C cells and larger ploidies

fc2x=1.0*c2x/num_final_cells # fraction of giant cells
fc3x=1.0*c3x/num_final_cells

output_obs=str(num_final_cells)+' '+str(c0)+' '+str(c1)+' '+str(c2)+' '+str(c3)+' '+str(c4)+' '+str(c5)+' '+str(c2x)+' '+str(fc2x)+' '+str(fc3x)+'\n';

filename="output_obs.txt";
obs_file = open (filename,'w');
obs_file.write(output_obs);
obs_file.close();


dirout='plots_ploidies_and_histograms'
try:
	os.stat(dirout)
except:
	os.mkdir(dirout)

for ii in range(0,len(vars_histoploidies),1) :
	varname=vars_histoploidies[ii];
	xmin=0;

	xmax=max(x[varname].values)*1.05;

	ymin=0;
	# note this is also computed later on in the script...
	sampconcs=x[x.step==num_steps]['atml1'].values;
	sampareas=x[x.step==num_steps]['area'].values;
	md=np.median(sampconcs)
	mn=np.mean(sampconcs)
	st=np.std(sampconcs)
	cv=st/mn
	sk=scipy.stats.skew(sampconcs)

	pylab.figure()
	plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)
	sample=x[x.step==num_steps][varname].values
	hist(sample, bins=nbins,alpha=0.75,range=(0,xmax),color='blue')

	plt.xlabel(xlabels[varname])
	plt.ylabel('Cell number')
	plt.axis([xmin, xmax,ymin, ymax])
	plt.text(xmax*0.65, ymax*0.82, r'CV='+str(format(cv, '.3f')), fontsize=fz)

	fileout='_'+varname
	fname="histo_all_"+fileout+".pdf"
	fullfname=dirout+'/'+fname
	plt.savefig(fullfname, format=None)
	plt.close()

num_2C = len(x[(x.step==num_steps) & (x.ploidy==0)].cell_id.unique());
num_4C = len(x[(x.step==num_steps) & (x.ploidy==1)].cell_id.unique());
num_8C = len(x[(x.step==num_steps) & (x.ploidy==2)].cell_id.unique());
num_16C = len(x[(x.step==num_steps) & (x.ploidy==3)].cell_id.unique());
num_32C = len(x[(x.step==num_steps) & (x.ploidy==4)].cell_id.unique());
num_64C = len(x[(x.step==num_steps) & (x.ploidy==5)].cell_id.unique());

pylab.figure()
plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

y = [num_2C, num_4C, num_8C, num_16C, num_32C, num_64C]

xx = [0,1,2,3,4,5]
xxtick = [0.33,1.33,2.33,3.33,4.33,5.33]

width = 1/1.5
plt.bar(xx, y, width, color="blue")

plt.xticks(xxtick, (r"2C",r"4C",r"8C",r"16C",r"32C",r"64C"),fontsize=fz)
plt.xlabel('Ploidy')
plt.ylabel('Cell number')

fname="bar_ploidies.pdf"
fullfname=dirout+'/'+fname
plt.savefig(fullfname, format=None)
plt.close()

pylab.figure()
plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

y = [100.0*num_2C/num_final_cells, 100.0*num_4C/num_final_cells, 100.0*num_8C/num_final_cells, 100.0*num_16C/num_final_cells, 100.0*num_32C/num_final_cells, 100.0*num_64C/num_final_cells]

xx = [0,1,2,3,4,5]
xxtick = [0.33,1.33,2.33,3.33,4.33,5.33]

width = 1/1.5
plt.bar(xx, y, width, color="blue")

plt.xticks(xxtick, (r"2C",r"4C",r"8C",r"16C",r"32C",r"64C"),fontsize=fz)
plt.xlabel('Ploidy')
plt.ylabel('Percentage')

fname="bar_percentage_ploidies.pdf"
fullfname=dirout+'/'+fname
plt.savefig(fullfname, format=None)
plt.close()

if auto==0 :
	sampconcs=x[x.step==num_steps]['atml1'].values;
	sampareas=x[x.step==num_steps]['area'].values;

	sample=np.multiply(sampconcs,sampareas)
	sm=np.sum(sample)
	md=np.median(sampconcs)

	mn=np.mean(sampconcs)

	st=np.std(sampconcs)
	cv=st/mn

	sk=scipy.stats.skew(sampconcs)
	max_ploidy=x['ploidy'].max();
	min_ploidy=x[x.step==num_steps]['ploidy'].min();
	giantcellsploid = x[(x.step==num_steps) & (x.ploidy>1)].cell_id.unique();

	nongiant_finalcells=x[(x.step==num_steps) & (x.ploidy<2)].cell_id.unique();
	num_giantcells=len(giantcellsploid)
	frgiant=1.0*num_giantcells/num_final_cells

	obs_final=[(cv,sm,mn,md,st,sk,num_2C,num_4C,num_8C,num_16C,num_32C,num_final_cells,num_giantcells,frgiant)]
	x_obs=pd.DataFrame(obs_final, columns=['CV','SUMS','MEANS','MEDIANS','STDS','SKS','num_2C','num_4C','num_8C','num_16C','num_32C','num_final_cells','num_giantcells','fracgiant'])
	fullfilename=dirini+'/obs.dat'
	x_obs.to_csv(fullfilename, sep='\t',  header=True, index=True, index_label=None, line_terminator='\n', date_format=None, decimal='.')
	print('cv=',cv)

	common_ids=np.intersect1d(x[x.step==step_points[-1]]['cell_id'],x[x.step==step_points[-2]]['cell_id'])


	ij=0

	samp_areas=[]
	samp_growthrates=[]
	zs=[]
	for ij in range(0,len(common_ids)) :
		i_area=x[(x.step==step_points[-1]) & (x.cell_id==common_ids[ij])]['area'].values
		prev_i_area=x[(x.step==step_points[-2]) & (x.cell_id==common_ids[ij])]['area'].values
		i_growthrate=(i_area-prev_i_area)/dt;
		i_zs=x[(x.step==step_points[-1]) & (x.cell_id==common_ids[ij])]['ploidy'].values
		samp_areas.append(i_area)
		samp_growthrates.append(i_growthrate)
		zs.append(i_zs)

	pylab.figure()
	plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)
	#plt.scatter(samp_areas,samp_growthrates, c=col,lw=0.2, cmap=cmap,s=100,alpha=0.7,zorder=1000, vmin=0, vmax=5)
	plt.scatter(samp_areas,samp_growthrates, lw=0.2, cmap=cmap,s=100,alpha=0.7,zorder=1000, vmin=0, vmax=5)

	plt.ylabel('Growth rate [AU]')
	plt.xlabel('Areas [AU]')
	fname="scatt_growth_rate_vs_area.pdf"
	fullfname=dirout+'/'+fname
	#print(fullfname)
	plt.savefig(fullfname, format=None)
	plt.close()

labels_vs_t=['time','num_2C','num_4C','num_8C','num_16C','num_32C','num_64C','num_step_cells','num_giantcells_old','frgiant_old','num_giantcells','frgiant','num_nongiantcells']

ind=0

for ii in printed_steps :
	time = x[x.step==ii]['time'].unique();
	time=time[0]

	num_2C = len(x[(x.step==ii) & (x.ploidy==0)].cell_id.unique());
	num_4C = len(x[(x.step==ii) & (x.ploidy==1)].cell_id.unique());
	num_8C = len(x[(x.step==ii) & (x.ploidy==2)].cell_id.unique());
	num_16C = len(x[(x.step==ii) & (x.ploidy==3)].cell_id.unique());
	num_32C = len(x[(x.step==ii) & (x.ploidy==4)].cell_id.unique());
	num_64C = len(x[(x.step==ii) & (x.ploidy==5)].cell_id.unique());


	num_step_cells= len(x[x.step==ii].cell_id.unique());

	num_giantcells_old= len(x[(x.step==ii) & (x.ploidy>2)].cell_id.unique());
	num_giantcells= len(x[(x.step==ii) & (x.ploidy>1)].cell_id.unique());
	num_nongiantcells=len(x[(x.step==ii) & (x.ploidy<2)].cell_id.unique());

	frgiant_old= 1.0*num_giantcells_old/num_step_cells;
	frgiant= 1.0*num_giantcells/num_step_cells;

	obs_vs_t=[(time,num_2C,num_4C,num_8C,num_16C,num_32C,num_64C,num_step_cells,num_giantcells_old,frgiant_old,num_giantcells,frgiant,num_nongiantcells)]

	x_obs_vs_t=pd.DataFrame(obs_vs_t, columns=labels_vs_t)

	if ind==0 :
		xpd_obs_vs_t=x_obs_vs_t
	else :
		xpd_obs_vs_t=pd.concat([xpd_obs_vs_t,x_obs_vs_t])

	ind=ind+1


tts=xpd_obs_vs_t['time'].values
ync=xpd_obs_vs_t['num_step_cells'].values
y2c=xpd_obs_vs_t['num_2C'].values
y4c=xpd_obs_vs_t['num_4C'].values
y8c=xpd_obs_vs_t['num_8C'].values
y16c=xpd_obs_vs_t['num_16C'].values
y32c=xpd_obs_vs_t['num_32C'].values

ygiant=xpd_obs_vs_t['num_giantcells'].values
ynongiant=xpd_obs_vs_t['num_nongiantcells'].values

y_fr=xpd_obs_vs_t['frgiant'].values
y_fr_old=xpd_obs_vs_t['frgiant_old'].values

fig=plt.figure()
ax = fig.add_subplot(111)

#pylab.figure()
plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

plt.plot(tts,ync,'k-',linewidth=lw,label="Total")
plt.plot(tts,y2c,'-',linewidth=lw,label="2C",color=colorlist[0])
plt.plot(tts,y4c,'-',linewidth=lw,label="4C",color=colorlist[1])
plt.plot(tts,y8c,'-',linewidth=lw,label="8C",color=colorlist[2])
plt.plot(tts,y16c,'-',linewidth=lw,label="16C",color=colorlist[3])
plt.plot(tts,y32c,'-',linewidth=lw,label="32C",color=colorlist[4])

ax.set_yscale('log')

#plt.axis([xmin, axxmax,ymin, ymax])
#plt.yticks(np.arange(0,ymax*1.01,ymax/4.0), (r"0",r"0.25",r"0.5",r"0.75",r"1"))
plt.legend(loc='upper right',prop={'size': 10})
#plt.legend(bbox_to_anchor=(0.4, 0.87),bbox_transform=plt.gcf().transFigure)
plt.xlabel('Time [AU]')
plt.ylabel('Cell number')

fileout="_ploidy_"
fname="tc"+fileout+".pdf"
fullfname=dirout+'/'+fname
plt.savefig(fullfname, format=None)
plt.close()


fig=plt.figure()
ax = fig.add_subplot(111)

#pylab.figure()
plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

plt.plot(tts,ync,'k-',linewidth=lw,label="Total")
plt.plot(tts,ynongiant,'-',linewidth=lw,label="Non giant cells",color=colorlist[0])
plt.plot(tts,ygiant,'-',linewidth=lw,label="Giant cells",color=colorlist[1])
ax.set_yscale('log')

#plt.axis([xmin, axxmax,ymin, ymax])
#plt.yticks(np.arange(0,ymax*1.01,ymax/4.0), (r"0",r"0.25",r"0.5",r"0.75",r"1"))
plt.legend(loc='upper right',prop={'size': 10})
	#plt.legend(bbox_to_anchor=(0.4, 0.87),bbox_transform=plt.gcf().transFigure)
plt.xlabel('Time [AU]')
plt.ylabel('Cell number')

fileout="_celltypes_"
fname="tc"+fileout+".pdf"
fullfname=dirout+'/'+fname
plt.savefig(fullfname, format=None)
plt.close()
print(dirout)


plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

plt.plot(tts,y_fr,'k-',linewidth=lw,label="Giant (8C or higher)")
plt.plot(tts,y_fr_old,'k--',linewidth=lw,label="Giant (16C or higher)")

ax.set_yscale('log')

#plt.axis([xmin, axxmax,ymin, ymax])
#plt.yticks(np.arange(0,ymax*1.01,ymax/4.0), (r"0",r"0.25",r"0.5",r"0.75",r"1"))
plt.legend(loc='upper right',prop={'size': 10})
	#plt.legend(bbox_to_anchor=(0.4, 0.87),bbox_transform=plt.gcf().transFigure)
plt.xlabel('Time [AU]')
plt.ylabel('Cell fraction')

fileout="_fr_ploidy_"
fname="tc"+fileout+".pdf"
fullfname=dirout+'/'+fname
plt.savefig(fullfname, format=None)
plt.close()
